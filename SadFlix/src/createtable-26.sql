DROP TABLE IF EXISTS `movies`;
CREATE TABLE `movies`
(	`id` INTEGER AUTO_INCREMENT NOT NULL PRIMARY KEY,
	`title` VARCHAR(100) NOT NULL, INDEX USING HASH (`title` (100)),
	`year` INTEGER NOT NULL, INDEX USING HASH (`year`),
	`director` VARCHAR(100) NOT NULL, INDEX USING HASH (`director` (100)),
	`banner_url` VARCHAR(200),
	`trailer_url` VARCHAR(200)
)
ENGINE=InnoDB;

DROP TABLE IF EXISTS `stars`;
CREATE TABLE `stars`
(	`id` INTEGER AUTO_INCREMENT NOT NULL PRIMARY KEY,
	`first_name` VARCHAR(50) NOT NULL, INDEX USING HASH (`first_name` (50)),
	`last_name` VARCHAR(50) NOT NULL, INDEX USING HASH (`last_name` (50)),
	`dob` DATE,
	`photo_url` VARCHAR(200),
	FULLTEXT INDEX `name` (`first_name`,`last_name`)
)
ENGINE=InnoDB;

DROP TABLE IF EXISTS `stars_in_movies`;
CREATE TABLE `stars_in_movies`
(	`star_id` INTEGER NOT NULL, FOREIGN KEY(`star_id`) REFERENCES `stars`(`id`) ON DELETE CASCADE,
	`movie_id` INTEGER NOT NULL, FOREIGN KEY(`movie_id`) REFERENCES `movies`(`id`) ON DELETE CASCADE,
	-- `star_id` INTEGER NOT NULL REFERENCES `stars` (`id`) ON DELETE CASCADE,
	-- `movie_id` INTEGER NOT NULL REFERENCES `movies` (`id`) ON DELETE CASCADE,
	PRIMARY KEY (`star_id`,`movie_id`)
)
ENGINE=InnoDB;

DROP TABLE IF EXISTS `genres`;
CREATE TABLE `genres`
(	`id` INTEGER AUTO_INCREMENT NOT NULL PRIMARY KEY,
	`name` VARCHAR(32) NOT NULL
)
ENGINE=InnoDB;

DROP TABLE IF EXISTS `genres_in_movies`;
CREATE TABLE `genres_in_movies`
(	`genre_id` INTEGER NOT NULL REFERENCES `genres` (`id`) ON DELETE CASCADE,
	`movie_id` INTEGER NOT NULL REFERENCES `movies` (`id`) ON DELETE CASCADE,
	PRIMARY KEY (`genre_id`,`movie_id`)
)
ENGINE=InnoDB;

DROP TABLE IF EXISTS `creditcards`;
CREATE TABLE `creditcards`
(	`id` VARCHAR(20) PRIMARY KEY,
	`first_name` VARCHAR(50) NOT NULL,
	`last_name` VARCHAR(50) NOT NULL,
	`expiration` DATE NOT NULL
)
ENGINE=InnoDB;

DROP TABLE IF EXISTS `customers`;
CREATE TABLE `customers`
(	`id` INT AUTO_INCREMENT PRIMARY KEY,
	`first_name` VARCHAR(50) NOT NULL,
	`last_name`  VARCHAR(50) NOT NULL,
	`cc_id`      VARCHAR(20) NOT NULL,
	`address`	 VARCHAR(200) NOT NULL,
	`email`     VARCHAR(50) NOT NULL,
	`password`    VARCHAR(20) NOT NULL,
	FOREIGN KEY(cc_id) REFERENCES creditcards(id)
)
ENGINE=InnoDB;

DROP TABLE IF EXISTS `sales`;
CREATE TABLE `sales`
(	`id` INT AUTO_INCREMENT PRIMARY KEY,
	`customer_id`   INT NOT NULL,
	`movie_id`      INT NOT NULL, 
	`sale_date`     DATE NOT NULL,
	FOREIGN KEY(customer_id) REFERENCES customers(id),
	FOREIGN KEY(movie_id) REFERENCES movies(id)
)
ENGINE=InnoDB;

