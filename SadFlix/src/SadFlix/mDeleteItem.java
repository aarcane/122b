package SadFlix;

import java.io.IOException;
import java.io.InputStream;
import java.io.PrintStream;
import java.sql.Connection;
import java.sql.SQLException;

public class mDeleteItem implements menuItem {

	java.sql.PreparedStatement deleteItem;
	
	@Override
	public int prepare(Connection conn) {
		
		// Prepare SQL statement template that’s to be repeatedly executed
		String newDeleteItem = "DELETE  `stars` (`first_name`, `last_name`, `dob`, `photo_url`) VALUES(?, ?, ?, ?);";
		
		try {
			deleteItem = conn.prepareStatement(newDeleteItem);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return 0;
	}

	@Override
	public int run(SadFlix S, Connection conn, PrintStream out, InputStream in) {
		
		String temp;
		
		System.out.print("Delete Item: ");
		
		String firstName;
		String lastName;
		String email = null;
		
		try {
			System.out.print("Name: ");
			
			temp = S.br.readLine();
			S.br.skip(in.available());
			
			String[] names = temp.split(" ",2);
			
			if(names.length == 2)
			{	firstName = names[0];
				lastName = names[1];
			}
			else
			{	lastName = names[0];
				firstName = "";
			}

			//insertCustomer.setString(0, id);
			deleteItem.setString(0, email);
			deleteItem.setString(1, firstName);
			deleteItem.setString(2, lastName);
			deleteItem.setString(3,email);
			deleteItem.executeUpdate();
			
		} catch (IOException e)
		{	System.err.println("Error");
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return 0;
	}

	@Override
	public String displayName() {
		return "Delete Item";
	}

}
