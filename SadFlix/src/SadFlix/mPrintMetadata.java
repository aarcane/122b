package SadFlix;

import java.io.BufferedReader;
import java.io.PrintStream;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.SQLException;

public class mPrintMetadata implements menuItem 
{
	
	InputStream in2;
	BufferedReader br2;

	@Override
	public int prepare(Connection conn) {
		
		// Prepare SQL statement template that's to be repeatedly executed
		
		/*try {
			printOutID = conn.prepareStatement("WHERE stars.id = ?;");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}*/
		
		return 0;
	}

	@Override
	public int run(SadFlix S, Connection conn, PrintStream out, InputStream in) {
		
		try {
			
			System.out.println("creditcards:" + "\t");
			//java.sql.PreparedStatement current = conn.prepareStatement("describe creditcards");
			java.sql.PreparedStatement current = conn.prepareStatement(
					"SELECT column_name, column_type " +
							"FROM INFORMATION_SCHEMA.COLUMNS " +
							"WHERE TABLE_NAME = 'creditcards';");
			runQuery(S, conn, current, "creditcards");
			
			System.out.println("customers:" + "\t");
			//current = conn.prepareStatement("describe customers");
			current = conn.prepareStatement(
					"SELECT column_name, column_type " +
							"FROM INFORMATION_SCHEMA.COLUMNS " +
							"WHERE TABLE_NAME = 'customers';");
			runQuery(S, conn, current, "customers");
			
			System.out.println("genres:" + "\t");
			//current = conn.prepareStatement("describe genres");
			current = conn.prepareStatement(
					"SELECT column_name, column_type " +
							"FROM INFORMATION_SCHEMA.COLUMNS " +
							"WHERE TABLE_NAME = 'genres';");
			runQuery(S, conn, current, "genres");
			
			System.out.println("genres_in_movies:" + "\t");
			//current = conn.prepareStatement("describe genres_in_movies");
			current = conn.prepareStatement(
					"SELECT column_name, column_type " +
							"FROM INFORMATION_SCHEMA.COLUMNS " +
							"WHERE TABLE_NAME = 'genres_in_movies';");
			runQuery(S, conn, current, "genres_in_movies");
			
			System.out.println("movies:" + "\t");
			//current = conn.prepareStatement("describe movies");
			current = conn.prepareStatement(
					"SELECT column_name, column_type " +
							"FROM INFORMATION_SCHEMA.COLUMNS " +
							"WHERE TABLE_NAME = 'movies';");
			runQuery(S, conn, current, "movies");
			
			System.out.println("sales:" + "\t");
			//current = conn.prepareStatement("describe sales");
			current = conn.prepareStatement(
					"SELECT column_name, column_type " +
							"FROM INFORMATION_SCHEMA.COLUMNS " +
							"WHERE TABLE_NAME = 'sales';");
			runQuery(S, conn, current, "sales");
			
			System.out.println("stars:" + "\t");
			//current = conn.prepareStatement("describe stars");
			current = conn.prepareStatement(
					"SELECT column_name, column_type " +
							"FROM INFORMATION_SCHEMA.COLUMNS " +
							"WHERE TABLE_NAME = 'stars';");
			runQuery(S, conn, current, "stars");
			
			System.out.println("stars_in_movies:" + "\t");
			//current = conn.prepareStatement("describe stars_in_movies");
			current = conn.prepareStatement(
					"SELECT column_name, column_type " +
							"FROM INFORMATION_SCHEMA.COLUMNS " +
							"WHERE TABLE_NAME = 'stars_in_movies';");
			runQuery(S, conn, current, "stars_in_movies");
			
			System.out.println();
			
		} catch (SQLException e) {
			System.out.println("There's a temporary problem with the SQL server. Please try again later.");
			System.out.println("An error stack trace is as follows:");
			e.printStackTrace();
		}
				
		return 0;
	}
	
	private void runQuery(SadFlix S, Connection conn, java.sql.PreparedStatement p, String field) throws SQLException
	{
		java.sql.ResultSet r = p.executeQuery();
		
		while (r.next())
		{
			System.out.println("\t" + r.getString(1) + " (" + r.getString(2) + ")");
		}
		
		System.out.println();
	}

	@Override
	public String displayName() {
		return "Print Metadata";
	}

}