package SadFlix;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintStream;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.Date;
import java.sql.SQLException;

public class mAddMovieSP implements menuItem 
{

	InputStream in2;
	BufferedReader br2;

	java.sql.CallableStatement proc;

	@Override
	public int prepare(Connection conn) {

		// Prepare SQL statement template that's to be repeatedly executed

		/*try {
			printOutID = conn.prepareStatement("WHERE stars.id = ?;");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}*/

		return 0;
	}

	@Override
	public int run(SadFlix S, Connection conn, PrintStream out, InputStream in) {

		String temp;

		String title = "";
		int year = 0;
		String director = "";
		String banner_url = "";
		String trailer_url = "";

		String starfname = "";
		String starlname = "";
		Date stardob = null;

		String genre = "";

		try {
			System.out.print("Movie title: ");
			temp = S.br.readLine();
			S.br.skip(in.available());
			title = temp;

			System.out.print("Year: ");
			temp = S.br.readLine();
			S.br.skip(in.available());
			year = Integer.parseInt(temp);

			System.out.print("Director: ");
			temp = S.br.readLine();
			S.br.skip(in.available());
			director = temp;

			System.out.print("Banner URL (Optional): ");
			temp = S.br.readLine();
			S.br.skip(in.available());
			banner_url = temp;

			System.out.print("Trailer URL (Optional): ");
			temp = S.br.readLine();
			S.br.skip(in.available());
			trailer_url = temp;

			System.out.print("Star First Name: ");
			temp = S.br.readLine();
			S.br.skip(in.available());
			starfname = temp;

			System.out.print("Star Last Name: ");
			temp = S.br.readLine();
			S.br.skip(in.available());
			starlname = temp;

			System.out.print("Star DoB (yyyy-[m]m-[d]d): ");
			temp = S.br.readLine();
			S.br.skip(in.available());
			stardob = Date.valueOf(temp);

			System.out.print("Genre Name: ");
			temp = S.br.readLine();
			S.br.skip(in.available());
			genre = temp;


			java.sql.CallableStatement cs = conn.prepareCall("{call add_movie(?, ?, ?, ?, ?, ?, ?, ?, ?)}");
			cs.setString(1, title);
			cs.setInt(2, year);
			cs.setString(3, director);
			cs.setString(4, banner_url);
			cs.setString(5, trailer_url);
			cs.setString(6, starfname);
			cs.setString(7, starlname);
			cs.setDate(8, stardob);
			cs.setString(9, genre);

			cs.execute();
			
			System.out.println("Add movie procedure run. Please verify results.");
			System.out.println();
			

			/*while (rs.next())
			{

			}
			 */
		} catch (IOException e)
		{
			e.printStackTrace();
		} catch (SQLException e) {
			//e.printStackTrace();
		}

		return 0;
	}


	@Override
	public String displayName() {
		return "Add Movie";
	}

}