package SadFlix;

import java.io.IOException;
import java.io.InputStream;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.sql.*;

public class SadFlix
{	Connection connection;
	boolean cont;
	java.util.ArrayList<menuItem> M;
	public InputStream in;
	public BufferedReader br;
	public PrintStream out;

	private SadFlix()
	{	connection = null;
		cont = false;
		in = System.in;
		out = System.out;
		br = new BufferedReader(new InputStreamReader(in));
		
		M = new java.util.ArrayList<menuItem>();
		//Add all menuItem here in the order in which they should appear.
		M.add(new mPrintOut());
		M.add(new mAddMovieSP());
		M.add(new mInsertCustomer());
		M.add(new mInsertStar());
		M.add(new mDeleteItem());
		M.add(new mPrintMetadata());
		M.add(new mQuery());
		M.add(new mReconnect());
		M.add(new mQuit());
		
		try
		{	Class.forName("com.mysql.jdbc.Driver");
		} catch (ClassNotFoundException e)
		{	System.err.println("Unable to locate a suitable Database connector.");
		}
		
		return;
	}

	private int run()
	{	do
		{	getConnection();
			if(null!=connection && cont) showMenu();
		}while(cont);
		out.println("Thank you and have a nice day.");
		return 0;
	}
	
	private void getConnection()
	{	String jdbcHost = "fabflix.aarcane.info";
		String jdbcDBName = "moviedb";
		String jdbcUser = "fabflix";
		String jdbcPass = "thepasswordispassword";
		String temp = "";
		out.print("Specify advanced options? ");
		try
		{	temp = br.readLine();
			br.skip(in.available());
			if(temp.startsWith("y"))
			{	out.printf("Database Host [%s]: ", jdbcHost);
				temp = br.readLine();
				br.skip(in.available());
				if(!temp.isEmpty()) jdbcHost = temp;
				out.printf("Database name [%s]: ", jdbcDBName);
				temp = br.readLine();
				br.skip(in.available());
				if(!temp.isEmpty()) jdbcDBName = temp;
			}
			
			out.printf("Database User [%s]: ", jdbcUser);
			temp = br.readLine();
			br.skip(in.available());
			if(!temp.isEmpty()) jdbcUser = temp;
			out.printf("Database Password [%s]: ", jdbcPass);
			temp = br.readLine();
			br.skip(in.available());
			if(!temp.isEmpty()) jdbcPass = temp;
			
		} catch (IOException e1)
		{	// TODO Auto-generated catch block
			e1.printStackTrace();
			cont = false;
		}
		
		try
		{	connection = DriverManager.getConnection(String.format("jdbc:mysql://%s/%s",jdbcHost,jdbcDBName), jdbcUser, jdbcPass);
		} catch (SQLException e)
		{	System.err.println("Unable to connect to database.");
		}
		for(menuItem m: M) m.prepare(connection);
		
		cont = true;

	}
	
	private void showMenu()
	{	while(cont && connection != null)
		{	//display Menu
			for(int i = 0; i < M.size(); ++i)
				out.println("    " + i + ")\t" + M.get(i).displayName());
			
			out.print("Enter your selection: ");
			try {
				String temp = br.readLine();
				br.skip(in.available());
				int selection = Integer.parseInt(temp);
				if(selection >= 0 && selection < M.size())
					M.get(selection).run(this, connection, out, in);
			} catch (IOException | NumberFormatException e)
			{	//e.printStackTrace();
			}
		}
	}
	
	public static void main(String[] args)
	{	if((new SadFlix()).run() < 0)
			System.err.println("An error has occured.  Exiting abnormally.");
	}
	public void terminar(boolean term)
	{	cont = !term;
	}
	public void closeConnection()
	{	try
		{	connection.close();
		} catch (SQLException e)
		{
		} finally
		{	connection = null;
		}
		
	}
}
