package SadFlix;

import java.io.PrintStream;
import java.io.InputStream;
import java.sql.Connection;

public class mQuit implements menuItem
{	@Override
	public int prepare(Connection conn)
	{	// prepare any module specific statements here.
		return 0;
	}

	@Override
	public int run(SadFlix S, Connection conn, PrintStream out, InputStream in)
	{	// Whether you're running a loop to prompt for input, or dumping
		//   raw data to the CLI, do it here, and in any sub modules
		S.terminar(true);
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public String displayName()
	{	// This is simply a place to return the method name.
		//   Gets called AFTER prepare, so you can run a query
		//   if you feel you must...
		return "Quit";
	}

}
