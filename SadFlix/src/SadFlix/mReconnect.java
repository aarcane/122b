package SadFlix;

import java.io.PrintStream;
import java.io.InputStream;
import java.sql.Connection;

public class mReconnect implements menuItem
{	@Override
	public int prepare(Connection conn) { return 0; }

	@Override
	public int run(SadFlix S, Connection conn, PrintStream out, InputStream in)
	{	S.closeConnection();
		return 0;
	}

	@Override
	public String displayName()
	{	return "Reconnect";
	}

}
