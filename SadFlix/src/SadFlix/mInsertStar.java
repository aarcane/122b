package SadFlix;

import java.io.IOException;
import java.io.PrintStream;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.Date;
import java.sql.SQLException;


public class mInsertStar implements menuItem {
	java.sql.PreparedStatement insertStar;
	@Override
	public int prepare(Connection conn) {
		// Prepare SQL statement template that’s to be repeatedly executed
		String insertNewStar = "INSERT INTO  `stars` (`first_name`, `last_name`, `dob`, `photo_url`) VALUES(?, ?, ?, ?);";
		
		try {
			insertStar = conn.prepareStatement(insertNewStar);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return 0;
	}

	@Override
	public int run(SadFlix S, Connection conn, PrintStream out, InputStream in)
	{	// Prompt for input, execute the statements
		String temp;
		System.out.print("Insert star: ");
		String firstName;
		String lastName;
		Date dob;
		String photoURL;
		try {
			System.out.print("Name: ");
			temp = S.br.readLine();
			S.br.skip(in.available());
			String[] names = temp.split(" ",2);
			if(names.length == 2)
			{	firstName = names[0];
				lastName = names[1];
			}
			else
			{	lastName = names[0];
				firstName = "";
			}
			
			System.out.print("DoB (yyyy-[m]m-[d]d): ");
			temp = S.br.readLine();
			S.br.skip(in.available());
			dob = Date.valueOf(temp);
			
			System.out.print("Photo URL: ");
			temp = S.br.readLine();
			S.br.skip(in.available());
			photoURL = temp;
			
			insertStar.setString(1, firstName);
			insertStar.setString(2, lastName);
			insertStar.setDate(3, dob);
			insertStar.setString(4,photoURL);
			insertStar.executeUpdate();
			
		} catch (IOException e)
		{	System.err.println("What are you, some kinda idiot?");
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		

		
		return 0;
	}

	@Override
	public String displayName() {
		return "Insert Star";
	} 

}

