package SadFlix;

interface menuItem
{	/**
	 * Prepare any necessary statements for later execution within here.  That's almost literally all this method should do.  
	 * Any other setup should be done in the constructor, if defined.
	 * 
	 * @param conn The connection on which SQL statements should be prepared.
	 * @return Ignored.
	 */
	int prepare(java.sql.Connection conn);
	/**
	 * @param S A reference to SadFlix.  Generally passed as this;  Used for access to S.in, S.out, and S.br
	 * @param conn An SQL connection on which to execute queries.
	 * @param out Don't use this.  Pass S.out.
	 * @param in Don't use this.  Pass S.in.
	 * @return Ignored.
	 */
	int run(SadFlix S, java.sql.Connection conn, java.io.PrintStream out, java.io.InputStream in);
	/**
	 * Literally return the displayName for this class in menu listings.
	 * @return String representation of a title for this menuItem.
	 */
	String displayName();
}
