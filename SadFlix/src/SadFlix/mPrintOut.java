package SadFlix;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintStream;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.SQLException;

public class mPrintOut implements menuItem 
{
	
	InputStream in2;
	BufferedReader br2;
	java.sql.PreparedStatement printOutID;
	java.sql.PreparedStatement printOutAnd;
	java.sql.PreparedStatement printOutOr;
	
	String printOut0 = "SELECT `movies`.`id`,`title`,`year`,`director`,`banner_url`,`trailer_url` " +
								"FROM `movies` " +
								"INNER JOIN (`stars_in_movies`,`stars`) " +
								"	ON (movies.id = stars_in_movies.movie_id " +
								"	AND stars.id = stars_in_movies.star_id)";

	@Override
	public int prepare(Connection conn) {
		
		// Prepare SQL statement template that's to be repeatedly executed
		
		try {
			printOutID = conn.prepareStatement(printOut0 + " WHERE stars.id = ?;");
			printOutAnd = conn.prepareStatement(printOut0 + " WHERE stars.first_name LIKE ? AND stars.last_name LIKE ?;");
			printOutOr = conn.prepareStatement(printOut0 + " WHERE stars.first_name LIKE ? OR stars.last_name LIKE ?;");
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return 0;
	}

	@Override
	public int run(SadFlix S, Connection conn, PrintStream out, InputStream in) {
		
		// Prompt for input, execute the statements
				String temp;
				
				String firstName;
				String lastName;
				int starID;
				
				try {
					System.out.print("Enter name or ID: ");
					temp = S.br.readLine();
					S.br.skip(in.available());
					
					
					try {
						starID = Integer.parseInt(temp);
						
						printOutID.setInt(1, starID);
						runQuery(S, printOutID);
					}
					catch (NumberFormatException e)
					{
						String[] names = temp.split(" ", 2);
						
						if (names.length == 2)
						{
							firstName = names[0];
							lastName = names[1];
							
							printOutAnd.setString(1, "%" + firstName + "%");
							printOutAnd.setString(2, "%" + lastName + "%");
							runQuery(S, printOutAnd);
						}
						else
						{
							firstName = names[0];
							lastName = names[0];
							
							printOutOr.setString(1, "%" + firstName + "%");
							printOutOr.setString(2, "%" + lastName + "%");
							runQuery(S, printOutOr);
						}
					}
					
				} catch (IOException e)
				{
					System.err.println("Something has gone temporarily wrong with the input stream. Please try again later.");
					System.err.println("A stack trace is as follows:");
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (SQLException e) {
					System.err.println("Something has gone temporarily wrong with the SQL server. Please try again later.");
					System.err.println("A stack trace is as follows:");
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				return 0;
	}
	
	private void runQuery(SadFlix S, java.sql.PreparedStatement p) throws SQLException
	{
		java.sql.ResultSet r = p.executeQuery();
		
		while (r.next())
		{
			System.out.print("ID: ");
			System.out.println(r.getInt("id"));
			
			System.out.print("Title: ");
			System.out.println(r.getString("title"));
			
			System.out.print("Year: ");
			System.out.println(r.getInt("year"));
			
			System.out.print("Director: ");
			System.out.println(r.getString("director"));
			
			System.out.print("Banner URL: ");
			System.out.println(r.getString("banner_url"));
			
			System.out.print("Trailer URL: ");
			System.out.println(r.getString("trailer_url"));		
			
			System.out.println();
		}
	}

	@Override
	public String displayName() {
		return "Display Movies by Star";
	} 

}