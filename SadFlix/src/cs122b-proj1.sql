CREATE TABLE `customers`
(
	`id` INT AUTO_INCREMENT PRIMARY KEY,
	`cc_id`      VARCHAR(20),
	`first_name` VARCHAR(50) NOT NULL,
	`last_name`  VARCHAR(50) NOT NULL,
	`address`	 VARCHAR(200) NOT NULL,
	`email`     VARCHAR(50) NOT NULL,
	`password`    VARCHAR(20) NOT NULL,
	FOREIGN KEY(cc_id) REFERENCES creditcards(id)
);

CREATE TABLE `sales`
(
	`id` INT AUTO_INCREMENT PRIMARY KEY,
	`customer_id`   INT,
	`movie_id`      INT, 
	`sale_date`     DATE NOT NULL,
	FOREIGN KEY(customer_id) REFERENCES customers(id),
	FOREIGN KEY(movie_id) REFERENCES movies(id)
);

CREATE TABLE `creditcards`
(
	`id` VARCHAR(20) PRIMARY KEY,
	`first_name` VARCHAR(50) NOT NULL,
	`last_name` VARCHAR(50) NOT NULL,
	`expiration` DATE NOT NULL
);



