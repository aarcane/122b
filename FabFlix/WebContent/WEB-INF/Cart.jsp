<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ page import="java.sql.*,javax.annotation.Resource,javax.sql.DataSource,javax.naming.Context,javax.naming.InitialContext" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet" type="text/css" href="includes/default.css">
<title>Cart</title> 
</head>
<body>
<h1>Cart</h1>
<table width="92%">

<thead><tr><th>Movie Name</th><th>Picture</th><th>Remove</th></tr></thead>
<%	FabFlix.Cart C = (FabFlix.Cart)request.getAttribute("C0"); 
	C.initCartItems(request, response);
	Context initContext = new InitialContext();
	javax.naming.Context envCtx1 = (javax.naming.Context) initContext.lookup("java:comp/env");
	DataSource myDS = (DataSource)envCtx1.lookup("jdbc/TestDB");
	Connection dbcon = myDS.getConnection();
	PreparedStatement movie = dbcon.prepareStatement("SELECT id, title, banner_url FROM `movies` WHERE `id` = ? LIMIT 1;");

	for(Integer i:C.getCartItems())
	{	movie.setInt(1, i);
		ResultSet rs = movie.executeQuery();
		if(rs.first())
		{	//rs.next();
			%>
			<tr><td><%=rs.getString(2) %></td><td><a href="SingleMovie/<%=rs.getInt(1) %>"><img src="<%=rs.getString(3) %>" /></a></td><td><a href="<%=FabFlix.Cart.getREMOVEURL(rs.getInt(1)) %>">Remove</a></td></tr>
		<%	
		}
		
	}
%>
</table>
<p>Proceed to <a href="?checkout=1">Checkout</a></p>

</body>
</html>