<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@page import="FabFlix.Customer"%>
<jsp:useBean id="C0" class="FabFlix.Customer" scope="session" />
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet" type="text/css" href="includes/default.css">
<title>Login</title>
</head>
<body>
<h1>Welcome to FabFlix!</h1>
<h2>View Customer Data</h2>
<table width="92%">
<tr><th>Customer ID</th>	<td><jsp:getProperty name="C0" property="ID" /></td></tr>
<tr><th>First Name</th>		<td><jsp:getProperty name="C0" property="FIRSTNAME" /></td></tr>
<tr><th>Last Name</th>		<td><jsp:getProperty name="C0" property="LASTNAME" /></td></tr>
<tr><th>E-mail</th>			<td><jsp:getProperty name="C0" property="EMAIL" /></td></tr>
</table>

<Form action="#" method="post">
<label for="logout" style="visibility: hidden;">Logout</label>
<input type="submit" name="logout" id="logout" value="Logout" />
</Form>

</body>
</html>