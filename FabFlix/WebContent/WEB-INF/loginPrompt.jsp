<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet" type="text/css" href="includes/default.css">
<title>Login</title>
</head>
<body>
<h1>Welcome to FabFlix!</h1>
<h2>Login</h2>
<Form action="#" method="post">
<label for="email">E-mail</label>
<input type="text" name="email" id="email" />
<label for="password">Password</label>
<input type="password" name="password" id="password" />
<label for="login" class="submit">Login</label>
<input type="submit" name="login" id="login" value="Login" />
</Form>
<div class="status"><%= request.getParameter("email") %><br /><%= request.getParameter("password") %><br /><%= session.getAttribute("CustomerID") %> </div>
</body>
</html>