<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ page import="java.sql.*,javax.annotation.Resource,javax.sql.DataSource,javax.naming.Context,javax.naming.InitialContext" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet" type="text/css" href="includes/default.css">
<title>FabFlix Home</title>
</head>
<body>
<h1>Welcome to FabFlix!</h1>
<div id="header">
	<Form action="#" method="post">
		<label for="logout" class="submit">Logout</label>
		<input type="submit" name="logout" id="logout" value="Logout">
	</Form>
</div>
<h2>Browse or Search</h2>
<p>find your favourite movies below by browsing or searching!</p>
<div id='search'>
	<input type="radio" name="choice-hidden" id="choice-search">
	<label for="choice-search">Search for Movies</label>
	<FORM ACTION="Search" METHOD="GET" class="reveal-if-active">
		<p>Please fill in at least one form field.</p>
		<label for="title">Title:</label><INPUT TYPE="TEXT" NAME="title" ID="title">
		<label for="year">Year:</label><INPUT TYPE="TEXT" NAME="year" ID="year">
		<label for="director">Director:</label><INPUT TYPE="TEXT" NAME="director" ID="director">
		<label for="flname">Star First Name/Last Name:</label><INPUT TYPE="TEXT" NAME="flname" ID="flname">
		
		<label for="limit">Display limit:</label>
		<select name="limit" id="limit">
			<option value="10">10</option>
			<option value="25">25</option>
			<option value="50">50</option>
			<option value="100">100</option>
		</select>
		<INPUT TYPE="hidden" NAME="search" VALUE="search">
		<INPUT TYPE="hidden" NAME="start" VALUE="0">
		<INPUT TYPE="hidden" NAME="sort" VALUE="title">
	  
	    <label for="submit" class="submit">Submit</label><INPUT TYPE="SUBMIT" VALUE="Search" ID="submit">
	</FORM>
</div>
<div id="byTitle">
	<input type="radio" name="choice-hidden" id="choice-byTitle">
	<label for="choice-byTitle">Browse by Title</label>
	<div class="reveal-if-active">
	<%	for(char i='A'; i < 'Z'; ++i)
		{
	%>
		<a href="Search?browse=title&alphaLetter=<%=i %>"><%=i %></a> 	
	<%	}
	%>
	</div>
</div>
<div id="byGenre">
	<input type="radio" name="choice-hidden" id="choice-byGenre">
	<label for="choice-byGenre">Browse by Genre</label>
	<div class="reveal-if-active">
	<%
	try
	{	Context initContext = new InitialContext();
		javax.naming.Context envCtx1 = (javax.naming.Context) initContext.lookup("java:comp/env");
		DataSource myDS = (DataSource)envCtx1.lookup("jdbc/TestDB");

		Connection dbcon = myDS.getConnection();
		//Connection dbcon = DriverManager.getConnection(loginUrl, loginUser, loginPasswd);
		
		out.println(FabFlix.Search.displayAllGenres(dbcon));
		
	}
	catch (SQLException ex) {
		while (ex != null) {
			System.out.println ("SQL Exception (t):  " + ex.getMessage ());
			ex = ex.getNextException ();
		}  // end while
	}  // end catch SQLException

	%>
	</div>
</div>


</body>
</html>