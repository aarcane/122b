package FabFlix;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.annotation.Resource;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class Search
 */
@WebServlet("/Search")
public class Search extends HttpServlet {
	private static final long serialVersionUID = 1L;
	@Resource(name="jdbc/TestDB")
    private javax.sql.DataSource myDS;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html");    // Response mime type

		// Output stream to STDOUT
		PrintWriter out = response.getWriter();

		out.println("<HTML><HEAD><TITLE>Movie List</TITLE></HEAD>");
		out.println("<BODY><H1>Movie List</H1>");

		
		boolean sorting = false;
		int limitPerPage = 10;
		int start = 0;
		String URLPrev = "";
		String URLNext = "";
		
		try 
		{	start = Integer.parseInt(request.getParameter("start"));
			limitPerPage = Integer.parseInt(request.getParameter("limit"));
		}
		catch (NumberFormatException nfe)
		{
		}
		
		
		try
		{	Connection dbcon = myDS.getConnection();
			java.sql.PreparedStatement query = null;

			if(null != request.getParameter("search"))
			{
				sorting = true;
				String title = request.getParameter("title");
				String year = request.getParameter("year");
				String director = request.getParameter("director");
				String flname = request.getParameter("flname");
				
				
				String sort = request.getParameter("sort");
				
				if (sort.equals("year"))
					sort = "ORDER BY movies.year ASC";
				else if (sort.equals("yeardsc"))
					sort = "ORDER BY movies.year DESC";
				else if (sort.equals("titledsc"))
					sort = "ORDER BY movies.title DESC";
				else
					sort = "ORDER BY movies.title ASC";
	
				if ( (title.trim() == "")
						&& (year.trim() == "")
						&& (director.trim() == "")
						&& (flname.trim() == "") )
				{ // If all of the fields are empty, there are no results
					out.println("<HTML>" +
							"<HEAD><TITLE>" +
							"FabFlix: Error" +
							"</TITLE></HEAD>\n<BODY>" +
							"<P>Please enter at least one search term.</P>"
							+ "<P><a href=\"search.html\">Return to search</a></p></BODY></HTML>");
					return;
				}
	
				title = title.trim();
				year = year.trim();
				director = director.trim();
				flname = flname.trim();
				
				String[] names = flname.split(" ", 2);
				String fname = "";
				String lname = "";
				
				if (names.length == 2)
				{
					fname = names[0];
					lname = names[1];
				}
				else
				{
					fname = names[0];
					lname = names[0];
				}
	
				// Preparing the query
				if (names.length == 2)
				{
					query = dbcon.prepareStatement(
							"SELECT movies.id, movies.title, movies.year, movies.director, "
								+ "stars.id, stars.first_name, stars.last_name "
							+ "FROM movies JOIN stars_in_movies ON movies.id = stars_in_movies.movie_id "
							+ "JOIN stars ON stars.id = stars_in_movies.star_id "
							+ "WHERE movies.title LIKE ? "
							+ "AND movies.year LIKE ? "
							+ "AND movies.director LIKE ? "
							+ "AND (stars.first_name LIKE ? AND stars.last_name LIKE ?) "
							+ "GROUP BY movies.id "
							+ sort + " "
							+ "LIMIT " + start + "," + (limitPerPage+1) + ";"
							);
				}
				else
				{
					query = dbcon.prepareStatement(
							"SELECT movies.id, movies.title, movies.year, movies.director, "
								+ "stars.id, stars.first_name, stars.last_name "
							+ "FROM movies "
							+ "JOIN stars_in_movies ON movies.id = stars_in_movies.movie_id "
							+ "JOIN stars ON stars.id = stars_in_movies.star_id "
							+ "WHERE movies.title LIKE ? "
							+ "AND movies.year LIKE ? "
							+ "AND movies.director LIKE ? "
							+ "AND (stars.first_name LIKE ? OR stars.last_name LIKE ?) "
							+ "GROUP BY movies.id "
							+ sort + " "
							+ "LIMIT " + start + "," + (limitPerPage+1) + ";"
							);
				}
				
				query.setString(1, "%" + title + "%");
				query.setString(2, "%" + year + "%");
				query.setString(3, "%" + director + "%");
				query.setString(4, "%" + fname + "%");
				query.setString(5, "%" + lname + "%");
				
				
				out.println("<P>Searching for...</P>");
				
				out.println("<blockquote>Title: " + title + "<BR>");
				out.println("Year: " + year + "<BR>");
				out.println("Director: " + director + "<BR>");
				out.println("Star name (first/last): " + flname + "</blockquote>");

				int prevValue = start-limitPerPage;
				
				if ((start - limitPerPage) < 0)
					prevValue = 0;

				if (start != 0)
					URLPrev = " <a href=\"?title=" + title + "&year=" + year + "&director=" + director + "&flname=" + flname + "&limit=" + limitPerPage + "&search=search&start=" + prevValue + "&sort=" + sort + "\">Previous</a> ";
				URLNext = " <a href=\"?title=" + title + "&year=" + year + "&director=" + director + "&flname=" + flname + "&limit=" + limitPerPage + "&search=search&start=" + (start+limitPerPage) + "&sort=" + sort + "\">Next</a> ";
			}
			else if(null != request.getParameter("browse") && 
					request.getParameter("browse").equals("genre") && 
					null != request.getParameter("genre"))
			{	String genre = request.getParameter("genre");
				query = dbcon.prepareStatement(
							"SELECT movies.id, movies.title, movies.year, movies.director, "
							+ "stars.id, stars.first_name, stars.last_name "
							+ "FROM movies "
							+ "JOIN genres_in_movies ON movies.id = genres_in_movies.movie_id "
							+ "JOIN genres ON genres.id = genres_in_movies.genre_id "
							+ "JOIN stars_in_movies ON movies.id = stars_in_movies.movie_id "
							+ "JOIN stars ON stars.id = stars_in_movies.star_id "
							+ "WHERE genres.name = ? "
							+ "GROUP BY movies.id "
							+ "LIMIT " + (limitPerPage+1) + " OFFSET " + start + ";"
						);
				query.setString(1, genre);
				int prevValue = start-limitPerPage;
				if ((start - limitPerPage) < 0)
					prevValue = 0;
				if (start != 0)
					URLPrev = " <a href=\"?browse=genre&genre=" + genre + "&limit=" + limitPerPage + "&start=" + prevValue + "\">Previous</a> ";
				URLNext = " <a href=\"?browse=genre&genre=" + genre + "&limit=" + limitPerPage + "&start=" + (start+limitPerPage) + "\">Next</a> ";
			}
			else if(null != request.getParameter("browse") && 
					request.getParameter("browse").equals("title") && 
					null != request.getParameter("alphaLetter"))
			{	String alphaLetter = request.getParameter("alphaLetter");
				query = dbcon.prepareStatement(
							"SELECT movies.id, movies.title, movies.year, movies.director, "
							+ "stars.id, stars.first_name, stars.last_name "
							+ "FROM movies "
							+ "JOIN genres_in_movies ON movies.id = genres_in_movies.movie_id "
							+ "JOIN genres ON genres.id = genres_in_movies.genre_id "
							+ "JOIN stars_in_movies ON movies.id = stars_in_movies.movie_id "
							+ "JOIN stars ON stars.id = stars_in_movies.star_id "
							+ "WHERE movies.title LIKE ? "
							+ "GROUP BY movies.id "
							+ "LIMIT " + (limitPerPage+1) + " OFFSET " + start + ";"
						);
				query.setString(1, alphaLetter + "%");
				int prevValue = start-limitPerPage;
				if ((start - limitPerPage) < 0)
					prevValue = 0;
				if (start != 0)
					URLPrev = " <a href=\"?browse=title&alphaLetter=" + alphaLetter + "&limit=" + limitPerPage + "&start=" + prevValue + "\">Previous</a> ";
				URLNext = " <a href=\"?browse=title&alphaLetter=" + alphaLetter + "&limit=" + limitPerPage + "&start=" + (start+limitPerPage) + "\">Next</a> ";
			}

			java.sql.ResultSet rs;
			if(query == null)
			{
				out.println("<P>There is nothing to do.</P>");
			}
			else
			{
				rs = query.executeQuery();
				
				out.println("<p>Displaying..." + (start+1) + " through " + (start+limitPerPage) + "</p>");
				
				// Pagination
				out.println("<p>");
				
				
				out.println(URLPrev);
				
				//if ((start > limitPerPage) > 0)
				out.println(URLNext);
				
				out.println("</p>");
				// End pagination
			
				if (!rs.first())
					out.println("<P>There are no results.</P>");
				else
				{
					out.println("<TABLE WIDTH='100%' BORDER='1'>");
					
					out.println("<TH>ID</TH>");
					if(sorting)
					{	out.println("<TH>"
							+ "<a href=\"?title=" + request.getParameter("title") + "&year=" + request.getParameter("year") + "&director=" + request.getParameter("director") + "&flname=" + request.getParameter("flname") + "&limit=" + request.getParameter("limit") + "&search=search&start=" + request.getParameter("start") + "&sort=title\">Title</a>"
							+ " <a href=\"?title=" + request.getParameter("title") + "&year=" + request.getParameter("year") + "&director=" + request.getParameter("director") + "&flname=" + request.getParameter("flname") + "&limit=" + request.getParameter("limit") + "&search=search&start=" + request.getParameter("start") + "&sort=titledsc\">(Desc)</a>"
							+ "</TH>");
						out.println("<TH>"
							+ "<a href=\"?title=" + request.getParameter("title") + "&year=" + request.getParameter("year") + "&director=" + request.getParameter("director") + "&flname=" + request.getParameter("flname") + "&limit=" + request.getParameter("limit") + "&search=search&start=" + request.getParameter("start") + "&sort=year\">Year</a>"
							+ " <a href=\"?title=" + request.getParameter("title") + "&year=" + request.getParameter("year") + "&director=" + request.getParameter("director") + "&flname=" + request.getParameter("flname") + "&limit=" + request.getParameter("limit") + "&search=search&start=" + request.getParameter("start") + "&sort=yeardsc\">(Desc)</a>"
							+ "</TH>");
					}
					else
					{	out.println("<TH>Title</TH>");
						out.println("<TH>Year</TH>");
					}
					out.println("<TH>Director</TH>");
					out.println("<TH>Genre</TH>");
					out.println("<TH>Stars</TH>");
					out.println("<TH>Add to Cart</TH>");
					
					while (rs.next())
					{
						int rsid = rs.getInt("movies.id");
						String rstitle = rs.getString("movies.title");
						int rsyear = rs.getInt("movies.year");
						String rsdirector = rs.getString("movies.director");
						//String rsstarfn = rs.getString("stars.first_name");
						//String rsstarln = rs.getString("stars.last_name");
	
						out.println("<TR>");
						
						out.println("<TD align=\"center\">" + rsid + "</TD>");
						out.println("<TD align=\"center\"><a href=\"SingleMovie/" + rsid + "\">" + rstitle + "</a></TD>");
						out.println("<TD align=\"center\">" + rsyear + "</TD>");
						out.println("<TD align=\"center\">" + rsdirector +  "</TD>");
						out.println("<TD align=\"center\">" + displayGenres(dbcon, rsid) + "</TD>");
						out.println("<TD align=\"center\">" + displayStars(dbcon, rsid) + "</TD>");
						out.println("<TD align=\"center\"><a href=\"" + FabFlix.Cart.getADDURL(rsid) + "\">Add to Cart</a></TD>");
						
						out.println("</TR>");
					}
					
					out.println("</TABLE>");
				}
				
				rs.close();
			}
		}
		catch (SQLException ex) {
			while (ex != null) {
				System.out.println ("SQL Exception (t):  " + ex.getMessage ());
				ex = ex.getNextException ();
			}  // end while
		}  // end catch SQLException

		catch(java.lang.Exception ex)
		{
			out.println("<HTML>" +
					"<HEAD><TITLE>" +
					"MovieDB: Error" +
					"</TITLE></HEAD>\n<BODY>" +
					"<P>SQL error in doGet: " +
					ex.getMessage() + "</P></BODY></HTML>");
			
			return;
		}
		out.close();
	}// ? movieid=

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}
	
	private String displayGenres(Connection dbcon, int rsid) throws SQLException
	{
		java.sql.PreparedStatement query = dbcon.prepareStatement(
				"SELECT genres.name FROM genres "
					+ "JOIN genres_in_movies ON genres.id = genres_in_movies.genre_id "
					+ "JOIN movies ON movies.id = genres_in_movies.movie_id "
					+ "WHERE movies.id = ? "
					+ "ORDER BY genres.name;"
					);
		
		query.setInt(1, rsid);
		
		ResultSet rs = query.executeQuery();
		
		StringBuilder s = new StringBuilder();
		
		while (rs.next())
		{
			String rsgenre = rs.getString("genres.name");
			
			s.append(rsgenre + "<br /> ");
		}
		
		return s.toString();
	}
	
	private String displayStars(Connection dbcon, int rsid2) throws SQLException
	{
		java.sql.PreparedStatement query = dbcon.prepareStatement(
				"SELECT movies.id, stars.id, stars.first_name, stars.last_name "
					+ "FROM movies JOIN stars_in_movies ON movies.id = stars_in_movies.movie_id "
					+ "JOIN stars ON stars.id = stars_in_movies.star_id "
					+ "WHERE movies.id = ? "
					+ "ORDER BY stars.last_name;"
					);
		
		query.setInt(1, rsid2);
		
		ResultSet rs = query.executeQuery();
		
		StringBuilder s = new StringBuilder();
		
		while (rs.next())
		{
			int rsid = rs.getInt("stars.id");
			String rsfn = rs.getString("stars.first_name");
			String rsln = rs.getString("stars.last_name");
			
			s.append("<a href=\"SingleStar?id=" + rsid + "\">" + rsfn + " " + rsln + "</a><br /> ");
		}
		
		return s.toString();
	}
	public static String displayAllGenres(Connection dbcon) throws SQLException
	{
		java.sql.PreparedStatement query = dbcon.prepareStatement(
				"SELECT genres.name FROM genres "
					+ "ORDER BY genres.name;"
					);
		
		ResultSet rs = query.executeQuery();
		
		StringBuilder s = new StringBuilder();
		
		while (rs.next())
		{
			String rsgenre = rs.getString("genres.name");
			
			s.append("<a href=\"Search?browse=genre&genre=" + rsgenre + "\">" + rsgenre + "</a> ");
		}
		
		return s.toString();
	}

}
