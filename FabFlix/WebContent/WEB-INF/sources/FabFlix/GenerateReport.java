package FabFlix;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.annotation.Resource;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class GenerateReport
 */
@WebServlet("/GenerateReport")
public class GenerateReport extends HttpServlet {
	private static final long serialVersionUID = 1L;
	@Resource(name="jdbc/TestDB")
	private javax.sql.DataSource myDS;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public GenerateReport() {
		super();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html");		// Response mime type
		PrintWriter out = response.getWriter();		// Output stream to STDOUT - not needed, writing to HTML file instead

		// Create the HTML file to write report to
		java.io.File f = null;
		java.io.FileWriter fw = null;
		java.io.BufferedWriter w = null;

		out.println("<HTML><HEAD><TITLE>Unclean Data Report</TITLE></HEAD>");
		out.println("<BODY><H1>Unclean Data Report</H1>");

		try {
			f = new java.io.File("Report.html");
			fw = new java.io.FileWriter(f);
			w = new java.io.BufferedWriter(fw);

			String currentTime = new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(java.util.Calendar.getInstance().getTime());
			
			w.write("<HTML><HEAD><TITLE>Unclean Data Report</TITLE></HEAD>");
			w.write("<BODY><H1>Unclean Data Report</H1>");
			w.write("File generated on " + currentTime);
			w.newLine(); 
			
			out.println("Report generated on " + currentTime + " at: " + f.getAbsolutePath());
			

			try {

				Connection dbcon = myDS.getConnection();

				// Start fetching query results

				out.println("<h2>Movies without Stars</h2>");
				out.println("<p>Any existing movies (by unique ID) which do not have any stars associated with them are as follows.</p>");
				getMoviesWithNoStar(dbcon, out);
				
				w.write("<h2>Movies without Stars</h2>");
				w.write("<p>Any existing movies (by unique ID) which do not have any stars associated with them are as follows.</p>");
				getMoviesWithNoStar(dbcon, w);

				
				out.println("<h2>Stars without Movies</h2>");
				out.println("<p>Any existing stars (by unique ID) who do not have any movies associated with them are as follows.</p>");
				getStarsWithoutMovies(dbcon, out);
				
				w.write("<h2>Stars without Movies</h2>");
				w.write("<p>Any existing stars (by unique ID) who do not have any movies associated with them are as follows.</p>");
				getStarsWithoutMovies(dbcon, w);
				

				out.println("<h2>Genres without Movies</h2>");
				out.println("<p>Any existing genres (by unique ID) which do not have any movies associated with them are as follows.</p>");
				getGenresWithoutMovies(dbcon, out);
				
				w.write("<h2>Genres without Movies</h2>");
				w.write("<p>Any existing genres (by unique ID) which do not have any movies associated with them are as follows.</p>");
				getGenresWithoutMovies(dbcon, w);
				

				out.println("<h2>Movies without Genres</h2>");
				out.println("<p>Any existing movies (by unique ID) which do not have any genres associated with them are as follows.</p>");
				getMoviesWithoutGenres(dbcon, out);
				
				w.write("<h2>Movies without Genres</h2>");
				w.write("<p>Any existing movies (by unique ID) which do not have any genres associated with them are as follows.</p>");
				getMoviesWithoutGenres(dbcon, w);
				

				out.println("<h2>Stars Without First Name or Last Name</h2>");
				out.println("<p>Any existing stars (by unique ID) who are missing either a first name, last name, or both are as follows.</p>");
				getStarsWithoutFNLN(dbcon, out);
				
				w.write("<h2>Stars Without First Name or Last Name</h2>");
				w.write("<p>Any existing stars (by unique ID) who are missing either a first name, last name, or both are as follows.</p>");
				getStarsWithoutFNLN(dbcon, w);
				

				out.println("<h2>Possible Duplicate Movies</h2>");
				out.println("<p>Any existing movies (by unique ID) which may possibly be duplicates (title and year are the same, but listing ID is different) are as follows. Due to the nature of the search, possible entries may be listed twice (matched to the same values). Please review these entries manually.</p>");
				getSimilarMovies(dbcon, out);
				
				w.write("<h2>Possible Duplicate Movies</h2>");
				w.write("<p>Any existing movies (by unique ID) which may possibly be duplicates (title and year are the same, but listing ID is different) are as follows. Due to the nature of the search, possible entries may be listed twice (matched to the same values). Please review these entries manually.</p>");
				getSimilarMovies(dbcon, w);
				
				
				out.println("<h2>Possible Duplicate Stars</h2>");
				out.println("<p>Any existing stars (by unique ID) who may possibly be duplicates (name and date of birth are the same, but listing ID is different) are as follows. Due to the nature of the search, possible entries may be listed twice (matched to the same values). Please review these entries manually.</p>");
				getSimilarStars(dbcon, out);
				
				w.write("<h2>Possible Duplicate Stars</h2>");
				w.write("<p>Any existing stars (by unique ID) who may possibly be duplicates (name and date of birth are the same, but listing ID is different) are as follows. Due to the nature of the search, possible entries may be listed twice (matched to the same values). Please review these entries manually.</p>");
				getSimilarStars(dbcon, w);
				
				
				out.println("<h2>Possible Duplicate Genres</h2>");
				out.println("<p>Any existing genres (by unique ID) which may possibly be duplicates (name is the same, but listing ID is different) are as follows. Due to the nature of the search, possible entries may be listed twice (matched to the same values). Please review these entries manually.</p>");
				getSimilarGenres(dbcon, out);
				
				w.write("<h2>Possible Duplicate Genres</h2>");
				w.write("<p>Any existing genres (by unique ID) which may possibly be duplicates (name is the same, but listing ID is different) are as follows. Due to the nature of the search, possible entries may be listed twice (matched to the same values). Please review these entries manually.</p>");
				getSimilarGenres(dbcon, w);
				
				
				out.println("<h2>Stars with Improper/Old Dates of Birth</h2>");
				out.println("<p>Any existing stars (by unique ID) who have a birth date of before 1900, or later than today's date.</p>");
				getImproperDOBs(dbcon, out);
				
				w.write("<h2>Stars with Improper/Old Dates of Birth</h2>");
				w.write("<p>Any existing stars (by unique ID) who have a birth date of before 1900, or later than today's date.</p>");
				getImproperDOBs(dbcon, w);
				

				out.println("<h2>Customers with Improper Emails</h2>");
				out.println("<p>Any existing customers (by unique ID) whose email is missing the '@' sign.</p>");
				getImproperEmails(dbcon, out);
				
				w.write("<h2>Customers with Improper Emails</h2>");
				w.write("<p>Any existing customers (by unique ID) whose email is missing the '@' sign.</p>");
				getImproperEmails(dbcon, w);
			}
			catch (SQLException e) {
				out.println("<p>There seems to be a problem with connecting to the database. Please try again later.</p>");
				out.println("<p>A stack trace of the issue is as follows:</p>");

				e.printStackTrace();

				out.println("</BODY></HTML>");
				return;
			}	

			out.println("</BODY></HTML>");

			w.close();
			
		} catch (Exception e) {
			out.println("<p>There seems to be a problem with creating the report file. Please try again later.</p>");
			out.println("<p>A stack trace of the issue is as follows:</p>");

			e.printStackTrace();

			out.println("</BODY></HTML>");
			return;
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}



	// 1. Movies without star
	private void getMoviesWithNoStar(Connection dbcon, PrintWriter out) throws SQLException {

		java.sql.PreparedStatement query = dbcon.prepareStatement("SELECT movies.id, movies.title, movies.year, movies.director FROM movies WHERE movies.id NOT IN (SELECT DISTINCT stars_in_movies.movie_id FROM stars_in_movies) GROUP BY movies.id;");

		java.sql.ResultSet rs;
		rs = query.executeQuery();

		if (!rs.first())
			out.println("<P>There are no results.</P>");
		else
		{
			out.println("<TABLE WIDTH='100%' BORDER='1'>");

			out.println("<TR>");
			out.println("<TH width=\"15%\">ID</TH>");
			out.println("<TH>Title</TH>");
			out.println("<TH>Year</TH>");
			out.println("<TH>Director</TH>");
			out.println("<TH>Genre</TH>");
			out.println("</TR>");

			String rsid = rs.getString("movies.id");
			String rstitle = rs.getString("movies.title");
			String rsyear = rs.getString("movies.year");
			String rsdirector = rs.getString("movies.director");

			out.println("<TR>");

			out.println("<TD align=\"center\" width=\"15%\">" + rsid + "</TD>");
			out.println("<TD align=\"center\"><a href=\"SingleMovie?id=" + rsid + "\">" + rstitle + "</a></TD>");
			out.println("<TD align=\"center\">" + rsyear + "</TD>");
			out.println("<TD align=\"center\">" + rsdirector +  "</TD>");
			out.println("<TD align=\"center\">" + displayGenres(dbcon, rsid) + "</TD>");

			out.println("</TR>");

			while (rs.next())
			{
				rsid = rs.getString("movies.id");
				rstitle = rs.getString("movies.title");
				rsyear = rs.getString("movies.year");
				rsdirector = rs.getString("movies.director");

				out.println("<TR>");

				out.println("<TD align=\"center\">" + rsid + "</TD>");
				out.println("<TD align=\"center\"><a href=\"SingleMovie?id=" + rsid + "\">" + rstitle + "</a></TD>");
				out.println("<TD align=\"center\">" + rsyear + "</TD>");
				out.println("<TD align=\"center\">" + rsdirector +  "</TD>");
				out.println("<TD align=\"center\">" + displayGenres(dbcon, rsid) + "</TD>");

				out.println("</TR>");
			}

			out.println("</TABLE>");
		}

		rs.close();
	}

	// 2. stars without movies
	private void getStarsWithoutMovies(Connection dbcon, PrintWriter out) throws SQLException {
		java.sql.PreparedStatement query = dbcon.prepareStatement("SELECT stars.id, stars.first_name, stars.last_name, stars.dob FROM stars WHERE stars.id NOT IN (SELECT DISTINCT stars_in_movies.star_id FROM stars_in_movies);");

		java.sql.ResultSet rs;
		rs = query.executeQuery();

		if (!rs.first())
			out.println("<P>There are no results.</P>");
		else
		{
			out.println("<TABLE WIDTH='100%' BORDER='1'>");

			out.println("<TR>");
			out.println("<TH width=\"15%\">ID</TH>");
			out.println("<TH>Name</TH>");
			out.println("<TH>Date of Birth</TH>");
			out.println("</TR>");

			String rsid = rs.getString("stars.id");
			String rsfn = rs.getString("stars.first_name");
			String rsln = rs.getString("stars.last_name");
			String rsdob = rs.getString("stars.dob");

			if (!rsln.isEmpty())
				rsln = " " + rsln;

			out.println("<TR>");

			out.println("<TD align=\"center\" width=\"15%\">" + rsid + "</TD>");
			out.println("<TD align=\"center\"><a href=\"SingleStar?id=" + rsid + "\">" + rsfn + rsln + "</a></TD>");
			out.println("<TD align=\"center\">" + rsdob + "</TD>");

			out.println("</TR>");

			while (rs.next())
			{
				rsid = rs.getString("stars.id");
				rsfn = rs.getString("stars.first_name");
				rsln = rs.getString("stars.last_name");
				rsdob = rs.getString("stars.dob");

				if (!rsln.isEmpty())
					rsln = " " + rsln;

				out.println("<TR>");

				out.println("<TD align=\"center\" width=\"15%\">" + rsid + "</TD>");
				out.println("<TD align=\"center\"><a href=\"SingleStar?id=" + rsid + "\">" + rsfn + rsln + "</a></TD>");
				out.println("<TD align=\"center\">" + rsdob + "</TD>");

				out.println("</TR>");
			}

			out.println("</TABLE>");
		}

		rs.close();
	}

	// 3. genres without movies
	private void getGenresWithoutMovies(Connection dbcon, PrintWriter out) throws SQLException {
		java.sql.PreparedStatement query = dbcon.prepareStatement("SELECT genres.id, genres.name FROM genres JOIN genres_in_movies ON genres.id = genres_in_movies.genre_id JOIN movies WHERE movies.id NOT IN (SELECT genres_in_movies.movie_id FROM genres_in_movies) GROUP BY genres.id;");

		java.sql.ResultSet rs;
		rs = query.executeQuery();

		if (!rs.first())
			out.println("<P>There are no results.</P>");
		else
		{
			out.println("<TABLE WIDTH='100%' BORDER='1'>");

			out.println("<TR>");
			out.println("<TH width=\"15%\">ID</TH>");
			out.println("<TH>Name</TH>");
			out.println("</TR>");

			String rsid = rs.getString("genres.id");
			String rsname = rs.getString("genres.name");

			out.println("<TR>");

			out.println("<TD align=\"center\" width=\"15%\">" + rsid + "</TD>");
			out.println("<TD align=\"center\">" + rsname + "</TD>");

			out.println("</TR>");

			while (rs.next())
			{
				rsid = rs.getString("genres.id");
				rsname = rs.getString("genres.name");

				out.println("<TR>");

				out.println("<TD align=\"center\" width=\"15%\">" + rsid + "</TD>");
				// Below: does not work properly, as Search function searches by genre NAME and not ID
				//out.println("<TD align=\"center\"><a href=\"Search?browse=genre&genre=" + rsname + "\">" + rsname + "</a></TD>");
				out.println("<TD align=\"center\">" + rsname + "</TD>");

				out.println("</TR>");
			}

			out.println("</TABLE>");
		}

		rs.close();
	}

	// 4. Movies without genres
	private void getMoviesWithoutGenres(Connection dbcon, PrintWriter out) throws SQLException {
		java.sql.PreparedStatement query = dbcon.prepareStatement("SELECT movies.id, movies.title, movies.year FROM movies WHERE movies.id NOT IN (SELECT DISTINCT genres_in_movies.movie_id FROM genres_in_movies);");

		java.sql.ResultSet rs;
		rs = query.executeQuery();

		if (!rs.first())
			out.println("<P>There are no results.</P>");
		else
		{
			out.println("<TABLE WIDTH='100%' BORDER='1'>");

			out.println("<TR>");
			out.println("<TH width=\"15%\">ID</TH>");
			out.println("<TH>Title</TH>");
			out.println("<TH>Year</TH>");
			out.println("</TR>");

			String rsid = rs.getString("movies.id");
			String rstitle = rs.getString("movies.title");
			String rsyear = rs.getString("movies.year");

			out.println("<TR>");

			out.println("<TD align=\"center\" width=\"15%\">" + rsid + "</TD>");
			out.println("<TD align=\"center\"><a href=\"SingleMovie?id=" + rsid + "\">" + rstitle + "</a></TD>");
			out.println("<TD align=\"center\">" + rsyear + "</TD>");

			out.println("</TR>");

			while (rs.next())
			{
				rsid = rs.getString("movies.id");
				rstitle = rs.getString("movies.title");
				rsyear = rs.getString("movies.year");

				out.println("<TR>");

				out.println("<TD align=\"center\" width=\"15%\">" + rsid + "</TD>");
				out.println("<TD align=\"center\"><a href=\"SingleMovie?id=" + rsid + "\">" + rstitle + "</a></TD>");
				out.println("<TD align=\"center\">" + rsyear + "</TD>");

				out.println("</TR>");
			}

			out.println("</TABLE>");
		}

		rs.close();
	}

	// 5. Stars without fn, ln
	private void getStarsWithoutFNLN(Connection dbcon, PrintWriter out) throws SQLException {
		java.sql.PreparedStatement query = dbcon.prepareStatement("SELECT stars.id, stars.first_name, stars.last_name FROM stars WHERE stars.first_name = '' OR stars.last_name = '';");

		java.sql.ResultSet rs;
		rs = query.executeQuery();

		if (!rs.first())
			out.println("<P>There are no results.</P>");
		else
		{
			out.println("<TABLE WIDTH='100%' BORDER='1'>");

			out.println("<TR>");
			out.println("<TH width=\"15%\">ID</TH>");
			out.println("<TH>Name (if applicable)</TH>");
			out.println("</TR>");

			String rsid = rs.getString("stars.id");
			String rsfn = rs.getString("stars.first_name");
			String rsln = rs.getString("stars.last_name");

			if (rsfn.isEmpty() && rsln.isEmpty())
				rsln = " " + rsln;

			out.println("<TR>");

			out.println("<TD align=\"center\" width=\"15%\">" + rsid + "</TD>");
			out.println("<TD align=\"center\"><a href=\"SingleStar?id=" + rsid + "\">" + rsfn + "</a></TD>");

			out.println("</TR>");

			while (rs.next())
			{
				rsid = rs.getString("movies.id");
				rsfn = rs.getString("stars.first_name");
				rsln = rs.getString("stars.last_name");

				if (rsfn.isEmpty() && rsln.isEmpty())
					rsln = " " + rsln;

				out.println("<TR>");

				out.println("<TD align=\"center\" width=\"15%\">" + rsid + "</TD>");
				out.println("<TD align=\"center\"><a href=\"SingleStar?id=" + rsid + "\">" + rsfn + "</a></TD>");

				out.println("</TR>");
			}

			out.println("</TABLE>");
		}

		rs.close();
	}

	// 6. Expired credit cards
	private void getExpiredCCs(Connection dbcon, PrintWriter out) throws SQLException {
		java.sql.PreparedStatement query = dbcon.prepareStatement("SELECT creditcards.id, creditcards.first_name, customers.first_name, creditcards.expiration FROM creditcards JOIN customers ON creditcards.id = customers.cc_id WHERE creditcards.expiration >= CURDATE();");

		java.sql.ResultSet rs;
		rs = query.executeQuery();

		if (!rs.first())
			out.println("<P>There are no results.</P>");
		else
		{
			out.println("<TABLE WIDTH='100%' BORDER='1'>");

			out.println("<TR>");
			out.println("<TH width=\"15%\">ID</TH>");
			out.println("<TH>Name (if applicable)</TH>");
			out.println("</TR>");

			String rsid = rs.getString("stars.id");
			String rsfn = rs.getString("stars.first_name");
			String rsln = rs.getString("stars.last_name");

			if (rsfn.isEmpty() && rsln.isEmpty())
				rsln = " " + rsln;

			out.println("<TR>");

			out.println("<TD align=\"center\" width=\"15%\">" + rsid + "</TD>");
			out.println("<TD align=\"center\"><a href=\"SingleStar?id=" + rsid + "\">" + rsfn + "</a></TD>");

			out.println("</TR>");

			while (rs.next())
			{
				rsid = rs.getString("movies.id");
				rsfn = rs.getString("stars.first_name");
				rsln = rs.getString("stars.last_name");

				if (rsfn.isEmpty() && rsln.isEmpty())
					rsln = " " + rsln;

				out.println("<TR>");

				out.println("<TD align=\"center\" width=\"15%\">" + rsid + "</TD>");
				out.println("<TD align=\"center\"><a href=\"SingleStar?id=" + rsid + "\">" + rsfn + "</a></TD>");

				out.println("</TR>");
			}

			out.println("</TABLE>");
		}

		rs.close();
	}

	// 7. Movies, stars, genres nearly the same (group by id). M: name, year. S: fn, ln, dob
	private void getSimilarMovies(Connection dbcon, PrintWriter out) throws SQLException {
		java.sql.PreparedStatement query = dbcon.prepareStatement("SELECT m1.id, m2.id, m1.title, m2.title, m1.year, m2.year, m1.director, m2.director FROM movies as m1, movies as m2 WHERE m1.id <> m2.id AND m1.title = m2.title AND m1.year = m2.year GROUP BY m1.id;");

		java.sql.ResultSet rs;
		rs = query.executeQuery();

		if (!rs.first())
			out.println("<P>There are no results.</P>");
		else
		{
			out.println("<TABLE WIDTH='100%' BORDER='1'>");

			out.println("<TR>");
			out.println("<TH colspan=\"4\">Movie 1</TH>");
			out.println("<TH colspan=\"4\">Movie 2</TH>");
			out.println("</TR>");

			out.println("<TR>");
			out.println("<TH width=\"10%\">ID</TH>");
			out.println("<TH bgcolor=\"#eee\">Title</TH>");
			out.println("<TH bgcolor=\"#eee\">Year</TH>");
			out.println("<TH>Director</TH>");
			//out.println("<TH>Genres</TH>");
			//out.println("<TH>Stars</TH>");

			out.println("<TH width=\"10%\">ID</TH>");
			out.println("<TH bgcolor=\"#eee\">Title</TH>");
			out.println("<TH bgcolor=\"#eee\">Year</TH>");
			out.println("<TH>Director</TH>");
			//out.println("<TH>Genres</TH>");
			//out.println("<TH>Stars</TH>");
			out.println("</TR>");

			String rsid1 = rs.getString("m1.id");
			String rstitle1 = rs.getString("m1.title");
			String rsyear1 = rs.getString("m1.year");
			String rsdirector1 = rs.getString("m1.director");

			String rsid2 = rs.getString("m2.id");
			String rstitle2 = rs.getString("m2.title");
			String rsyear2 = rs.getString("m2.year");
			String rsdirector2 = rs.getString("m2.director");

			out.println("<TR>");

			out.println("<TD align=\"center\" width=\"10%\">" + rsid1 + "</TD>");
			out.println("<TD bgcolor=\"#eee\" align=\"center\"><a href=\"SingleMovie?id=" + rsid1 + "\">" + rstitle1 + "</a></TD>");
			out.println("<TD bgcolor=\"#eee\" align=\"center\">" + rsyear1 + "</TD>");
			out.println("<TD align=\"center\">" + rsdirector1 + "</TD>");
			//out.println("<TD align=\"center\">" + "genre" + "</TD>");
			//out.println("<TD align=\"center\">" + "stars"+ "</TD>");

			out.println("<TD align=\"center\" width=\"10%\">" + rsid2 + "</TD>");
			out.println("<TD bgcolor=\"#eee\" align=\"center\"><a href=\"SingleMovie?id=" + rsid2 + "\">" + rstitle2 + "</a></TD>");
			out.println("<TD bgcolor=\"#eee\" align=\"center\">" + rsyear2 + "</TD>");
			out.println("<TD align=\"center\">" + rsdirector2 + "</TD>");
			//out.println("<TD align=\"center\">" + "genre" + "</TD>");
			//out.println("<TD align=\"center\">" + "stars"+ "</TD>");

			out.println("</TR>");

			while (rs.next())
			{
				rsid1 = rs.getString("m1.id");
				rstitle1 = rs.getString("m1.title");
				rsyear1 = rs.getString("m1.year");
				rsdirector1 = rs.getString("m1.director");

				rsid2 = rs.getString("m2.id");
				rstitle2 = rs.getString("m2.title");
				rsyear2 = rs.getString("m2.year");
				rsdirector2 = rs.getString("m2.director");

				out.println("<TR>");

				out.println("<TD align=\"center\" width=\"10%\">" + rsid1 + "</TD>");
				out.println("<TD bgcolor=\"#eee\" align=\"center\"><a href=\"SingleMovie?id=" + rsid1 + "\">" + rstitle1 + "</a></TD>");
				out.println("<TD bgcolor=\"#eee\" align=\"center\">" + rsyear1 + "</TD>");
				out.println("<TD align=\"center\">" + rsdirector1 + "</TD>");
				//out.println("<TD align=\"center\">" + "genre" + "</TD>");
				//out.println("<TD align=\"center\">" + "stars"+ "</TD>");

				out.println("<TD align=\"center\" width=\"10%\">" + rsid2 + "</TD>");
				out.println("<TD bgcolor=\"#eee\" align=\"center\"><a href=\"SingleMovie?id=" + rsid2 + "\">" + rstitle2 + "</a></TD>");
				out.println("<TD bgcolor=\"#eee\" align=\"center\">" + rsyear2 + "</TD>");
				out.println("<TD align=\"center\">" + rsdirector2 + "</TD>");
				//out.println("<TD align=\"center\">" + "genre" + "</TD>");
				//out.println("<TD align=\"center\">" + "stars"+ "</TD>");

				out.println("</TR>");
			}

			out.println("</TABLE>");
		}

		rs.close();
	}
	
	private void getSimilarStars(Connection dbcon, PrintWriter out) throws SQLException {
		java.sql.PreparedStatement query = dbcon.prepareStatement("SELECT s1.id, s2.id, s1.first_name, s1.last_name, s2.first_name, s2.last_name, s1.dob, s2.dob FROM stars as s1, stars as s2 WHERE s1.id <> s2.id AND s1.first_name = s2.first_name AND s1.last_name = s2.last_name AND s1.dob = s2.dob GROUP BY s1.id;");

		java.sql.ResultSet rs;
		rs = query.executeQuery();

		if (!rs.first())
			out.println("<P>There are no results.</P>");
		else
		{
			out.println("<TABLE WIDTH='100%' BORDER='1'>");

			out.println("<TR>");
			out.println("<TH colspan=\"3\">Star 1</TH>");
			out.println("<TH colspan=\"3\">Star 2</TH>");
			out.println("</TR>");

			out.println("<TR>");
			out.println("<TH width=\"10%\">ID</TH>");
			out.println("<TH bgcolor=\"#eee\">Name</TH>");
			out.println("<TH bgcolor=\"#eee\">Date of Birth</TH>");
			
			out.println("<TH width=\"10%\">ID</TH>");
			out.println("<TH bgcolor=\"#eee\">Name</TH>");
			out.println("<TH bgcolor=\"#eee\">Date of Birth</TH>");
			out.println("</TR>");

			String rsid1 = rs.getString("s1.id");
			String rsfn1 = rs.getString("s1.first_name");
			String rsln1 = rs.getString("s1.last_name");
			String rsdob1 = rs.getString("s1.dob");
			
			if (!rsfn1.isEmpty() && !rsln1.isEmpty())
				rsln1 = " " + rsln1;

			String rsid2 = rs.getString("s2.id");
			String rsfn2 = rs.getString("s2.first_name");
			String rsln2 = rs.getString("s2.last_name");
			String rsdob2 = rs.getString("s2.dob");
			
			if (!rsfn2.isEmpty() && !rsln2.isEmpty())
				rsln2 = " " + rsln2;

			out.println("<TR>");

			out.println("<TD align=\"center\" width=\"10%\">" + rsid1 + "</TD>");
			out.println("<TD bgcolor=\"#eee\" align=\"center\"><a href=\"SingleStar?id=" + rsid1 + "\">" + rsfn1 + rsln1 + "</a></TD>");
			out.println("<TD bgcolor=\"#eee\" align=\"center\">" + rsdob1 + "</TD>");

			out.println("<TD align=\"center\" width=\"10%\">" + rsid2 + "</TD>");
			out.println("<TD bgcolor=\"#eee\" align=\"center\"><a href=\"SingleStar?id=" + rsid2 + "\">" + rsfn2 + rsln2 + "</a></TD>");
			out.println("<TD bgcolor=\"#eee\" align=\"center\">" + rsdob2 + "</TD>");

			out.println("</TR>");

			while (rs.next())
			{
				rsid1 = rs.getString("s1.id");
				rsfn1 = rs.getString("s1.first_name");
				rsln1 = rs.getString("s1.last_name");
				rsdob1 = rs.getString("s1.dob");
				
				if (!rsfn1.isEmpty() && !rsln1.isEmpty())
					rsln1 = " " + rsln1;

				rsid2 = rs.getString("s2.id");
				rsfn2 = rs.getString("s2.first_name");
				rsln2 = rs.getString("s2.last_name");
				rsdob2 = rs.getString("s2.dob");
				
				if (!rsfn2.isEmpty() && !rsln2.isEmpty())
					rsln2 = " " + rsln2;

				out.println("<TR>");

				out.println("<TD align=\"center\" width=\"10%\">" + rsid1 + "</TD>");
				out.println("<TD bgcolor=\"#eee\" align=\"center\"><a href=\"SingleStar?id=" + rsid1 + "\">" + rsfn1 + rsln1 + "</a></TD>");
				out.println("<TD bgcolor=\"#eee\" align=\"center\">" + rsdob1 + "</TD>");

				out.println("<TD align=\"center\" width=\"10%\">" + rsid2 + "</TD>");
				out.println("<TD bgcolor=\"#eee\" align=\"center\"><a href=\"SingleStar?id=" + rsid2 + "\">" + rsfn2 + rsln2 + "</a></TD>");
				out.println("<TD bgcolor=\"#eee\" align=\"center\">" + rsdob2 + "</TD>");

				out.println("</TR>");
			}

			out.println("</TABLE>");
		}

		rs.close();
	}
	
	private void getSimilarGenres(Connection dbcon, PrintWriter out) throws SQLException {
		java.sql.PreparedStatement query = dbcon.prepareStatement("SELECT g1.id, g2.id, g1.name, g2.name FROM genres as g1, genres as g2 WHERE g1.id <> g2.id AND g1.name = g2.name GROUP BY g1.id;");

		java.sql.ResultSet rs;
		rs = query.executeQuery();

		if (!rs.first())
			out.println("<P>There are no results.</P>");
		else
		{
			out.println("<TABLE WIDTH='100%' BORDER='1'>");

			out.println("<TR>");
			out.println("<TH colspan=\"2\">Genre 1</TH>");
			out.println("<TH colspan=\"2\">Genre 2</TH>");
			out.println("</TR>");

			out.println("<TR>");
			out.println("<TH width=\"10%\">ID</TH>");
			out.println("<TH bgcolor=\"#eee\">Name</TH>");
			
			out.println("<TH width=\"10%\">ID</TH>");
			out.println("<TH bgcolor=\"#eee\">Name</TH>");
			out.println("</TR>");

			String rsid1 = rs.getString("g1.id");
			String rsname1 = rs.getString("g1.name");

			String rsid2 = rs.getString("g2.id");
			String rsname2 = rs.getString("g2.name");

			out.println("<TR>");

			out.println("<TD align=\"center\" width=\"10%\">" + rsid1 + "</TD>");
			out.println("<TD bgcolor=\"#eee\" align=\"center\">" + rsname1 + "</TD>");

			out.println("<TD align=\"center\" width=\"10%\">" + rsid2 + "</TD>");
			out.println("<TD bgcolor=\"#eee\" align=\"center\">" + rsname2 + "</TD>");

			out.println("</TR>");

			while (rs.next())
			{
				rsid1 = rs.getString("g1.id");
				rsname1 = rs.getString("g1.name");

				rsid2 = rs.getString("g2.id");
				rsname2 = rs.getString("g2.name");

				out.println("<TR>");

				out.println("<TD align=\"center\" width=\"10%\">" + rsid1 + "</TD>");
				out.println("<TD bgcolor=\"#eee\" align=\"center\">" + rsname1 + "</TD>");

				out.println("<TD align=\"center\" width=\"10%\">" + rsid2 + "</TD>");
				out.println("<TD bgcolor=\"#eee\" align=\"center\">" + rsname2 + "</TD>");

				out.println("</TR>");
			}

			out.println("</TABLE>");
		}

		rs.close();
	}

	// 8. Birthdate > today, year < 1900
	private void getImproperDOBs(Connection dbcon, PrintWriter out) throws SQLException {
		java.sql.PreparedStatement query = dbcon.prepareStatement("SELECT stars.id, stars.first_name, stars.last_name, stars.dob FROM stars WHERE stars.dob >= CURDATE() OR stars.dob < '1900';");

		java.sql.ResultSet rs;
		rs = query.executeQuery();

		if (!rs.first())
			out.println("<P>There are no results.</P>");
		else
		{
			out.println("<TABLE WIDTH='100%' BORDER='1'>");

			out.println("<TR>");
			out.println("<TH width=\"10%\">ID</TH>");
			out.println("<TH>Name</TH>");
			out.println("<TH>Date of Birth</TH>");
			out.println("</TR>");

			String rsid = rs.getString("stars.id");
			String rsfn = rs.getString("stars.first_name");
			String rsln = rs.getString("stars.last_name");
			String rsdob = rs.getString("stars.dob");
			
			if (!rsfn.isEmpty() && !rsln.isEmpty())
				rsln = " " + rsln;

			out.println("<TR>");

			out.println("<TD align=\"center\" width=\"15%\">" + rsid + "</TD>");
			out.println("<TD align=\"center\">" + rsfn + rsln + "</TD>");
			out.println("<TD align=\"center\">" + rsdob + "</TD>");

			out.println("</TR>");

			while (rs.next())
			{
				rsid = rs.getString("stars.id");
				rsfn = rs.getString("stars.first_name");
				rsln = rs.getString("stars.last_name");
				rsdob = rs.getString("stars.dob");
				
				if (!rsfn.isEmpty() && !rsln.isEmpty())
					rsln = " " + rsln;

				out.println("<TR>");

				out.println("<TD align=\"center\" width=\"15%\">" + rsid + "</TD>");
				out.println("<TD align=\"center\">" + rsfn + rsln + "</TD>");
				out.println("<TD align=\"center\">" + rsdob + "</TD>");

				out.println("</TR>");
			}

			out.println("</TABLE>");
		}

		rs.close();
	}

	// 9. Customer email no @ sign
	private void getImproperEmails(Connection dbcon, PrintWriter out) throws SQLException {
		java.sql.PreparedStatement query = dbcon.prepareStatement("SELECT customers.id, customers.first_name, customers.last_name, customers.email FROM customers WHERE customers.email NOT LIKE '%@%';");

		java.sql.ResultSet rs;
		rs = query.executeQuery();

		if (!rs.first())
			out.println("<P>There are no results.</P>");
		else
		{
			out.println("<TABLE WIDTH='100%' BORDER='1'>");

			out.println("<TR>");
			out.println("<TH width=\"15%\">ID</TH>");
			out.println("<TH>Name</TH>");
			out.println("<TH>Email</TH>");
			out.println("</TR>");

			String rsid = rs.getString("customers.id");
			String rsfn = rs.getString("customers.first_name");
			String rsln = rs.getString("customers.last_name");
			String rsemail = rs.getString("customers.email");
			
			if (!rsfn.isEmpty() && !rsln.isEmpty())
				rsln = " " + rsln;

			out.println("<TR>");

			out.println("<TD align=\"center\" width=\"15%\">" + rsid + "</TD>");
			out.println("<TD align=\"center\">" + rsfn + rsln + "</TD>");
			out.println("<TD align=\"center\">" + rsemail + "</TD>");

			out.println("</TR>");

			while (rs.next())
			{
				rsid = rs.getString("customers.id");
				rsfn = rs.getString("customers.first_name");
				rsln = rs.getString("customers.last_name");
				rsemail = rs.getString("customers.email");
				
				if (!rsfn.isEmpty() && !rsln.isEmpty())
					rsln = " " + rsln;

				out.println("<TR>");

				out.println("<TD align=\"center\" width=\"15%\">" + rsid + "</TD>");
				out.println("<TD align=\"center\">" + rsfn + rsln + "</TD>");
				out.println("<TD align=\"center\">" + rsemail + "</TD>");

				out.println("</TR>");
			}

			out.println("</TABLE>");
		}

		rs.close();
	}
	
	// Write to file
	
	// 1. Movies without star
		private void getMoviesWithNoStar(Connection dbcon, java.io.BufferedWriter w) throws SQLException, IOException {

			java.sql.PreparedStatement query = dbcon.prepareStatement("SELECT movies.id, movies.title, movies.year, movies.director FROM movies WHERE movies.id NOT IN (SELECT DISTINCT stars_in_movies.movie_id FROM stars_in_movies) GROUP BY movies.id;");

			java.sql.ResultSet rs;
			rs = query.executeQuery();

			if (!rs.first())
				w.write("<P>There are no results.</P>");
			else
			{
				w.write("<TABLE WIDTH='100%' BORDER='1'>");

				w.write("<TR>");
				w.write("<TH width=\"15%\">ID</TH>");
				w.write("<TH>Title</TH>");
				w.write("<TH>Year</TH>");
				w.write("<TH>Director</TH>");
				w.write("<TH>Genre</TH>");
				w.write("</TR>");

				String rsid = rs.getString("movies.id");
				String rstitle = rs.getString("movies.title");
				String rsyear = rs.getString("movies.year");
				String rsdirector = rs.getString("movies.director");

				w.write("<TR>");

				w.write("<TD align=\"center\" width=\"15%\">" + rsid + "</TD>");
				w.write("<TD align=\"center\"><a href=\"SingleMovie?id=" + rsid + "\">" + rstitle + "</a></TD>");
				w.write("<TD align=\"center\">" + rsyear + "</TD>");
				w.write("<TD align=\"center\">" + rsdirector +  "</TD>");
				w.write("<TD align=\"center\">" + displayGenres(dbcon, rsid) + "</TD>");

				w.write("</TR>");

				while (rs.next())
				{
					rsid = rs.getString("movies.id");
					rstitle = rs.getString("movies.title");
					rsyear = rs.getString("movies.year");
					rsdirector = rs.getString("movies.director");

					w.write("<TR>");

					w.write("<TD align=\"center\">" + rsid + "</TD>");
					w.write("<TD align=\"center\"><a href=\"SingleMovie?id=" + rsid + "\">" + rstitle + "</a></TD>");
					w.write("<TD align=\"center\">" + rsyear + "</TD>");
					w.write("<TD align=\"center\">" + rsdirector +  "</TD>");
					w.write("<TD align=\"center\">" + displayGenres(dbcon, rsid) + "</TD>");

					w.write("</TR>");
				}

				w.write("</TABLE>");
			}

			rs.close();
		}

		// 2. stars without movies
		private void getStarsWithoutMovies(Connection dbcon, java.io.BufferedWriter w) throws SQLException, IOException {
			java.sql.PreparedStatement query = dbcon.prepareStatement("SELECT stars.id, stars.first_name, stars.last_name, stars.dob FROM stars WHERE stars.id NOT IN (SELECT DISTINCT stars_in_movies.star_id FROM stars_in_movies);");

			java.sql.ResultSet rs;
			rs = query.executeQuery();

			if (!rs.first())
				w.write("<P>There are no results.</P>");
			else
			{
				w.write("<TABLE WIDTH='100%' BORDER='1'>");

				w.write("<TR>");
				w.write("<TH width=\"15%\">ID</TH>");
				w.write("<TH>Name</TH>");
				w.write("<TH>Date of Birth</TH>");
				w.write("</TR>");

				String rsid = rs.getString("stars.id");
				String rsfn = rs.getString("stars.first_name");
				String rsln = rs.getString("stars.last_name");
				String rsdob = rs.getString("stars.dob");

				if (!rsln.isEmpty())
					rsln = " " + rsln;

				w.write("<TR>");

				w.write("<TD align=\"center\" width=\"15%\">" + rsid + "</TD>");
				w.write("<TD align=\"center\"><a href=\"SingleStar?id=" + rsid + "\">" + rsfn + rsln + "</a></TD>");
				w.write("<TD align=\"center\">" + rsdob + "</TD>");

				w.write("</TR>");

				while (rs.next())
				{
					rsid = rs.getString("stars.id");
					rsfn = rs.getString("stars.first_name");
					rsln = rs.getString("stars.last_name");
					rsdob = rs.getString("stars.dob");

					if (!rsln.isEmpty())
						rsln = " " + rsln;

					w.write("<TR>");

					w.write("<TD align=\"center\" width=\"15%\">" + rsid + "</TD>");
					w.write("<TD align=\"center\"><a href=\"SingleStar?id=" + rsid + "\">" + rsfn + rsln + "</a></TD>");
					w.write("<TD align=\"center\">" + rsdob + "</TD>");

					w.write("</TR>");
				}

				w.write("</TABLE>");
			}

			rs.close();
		}

		// 3. genres without movies
		private void getGenresWithoutMovies(Connection dbcon, java.io.BufferedWriter w) throws SQLException, IOException {
			java.sql.PreparedStatement query = dbcon.prepareStatement("SELECT genres.id, genres.name FROM genres JOIN genres_in_movies ON genres.id = genres_in_movies.genre_id JOIN movies WHERE movies.id NOT IN (SELECT genres_in_movies.movie_id FROM genres_in_movies) GROUP BY genres.id;");

			java.sql.ResultSet rs;
			rs = query.executeQuery();

			if (!rs.first())
				w.write("<P>There are no results.</P>");
			else
			{
				w.write("<TABLE WIDTH='100%' BORDER='1'>");

				w.write("<TR>");
				w.write("<TH width=\"15%\">ID</TH>");
				w.write("<TH>Name</TH>");
				w.write("</TR>");

				String rsid = rs.getString("genres.id");
				String rsname = rs.getString("genres.name");

				w.write("<TR>");

				w.write("<TD align=\"center\" width=\"15%\">" + rsid + "</TD>");
				w.write("<TD align=\"center\">" + rsname + "</TD>");

				w.write("</TR>");

				while (rs.next())
				{
					rsid = rs.getString("genres.id");
					rsname = rs.getString("genres.name");

					w.write("<TR>");

					w.write("<TD align=\"center\" width=\"15%\">" + rsid + "</TD>");
					// Below: does not work properly, as Search function searches by genre NAME and not ID
					//w.write("<TD align=\"center\"><a href=\"Search?browse=genre&genre=" + rsname + "\">" + rsname + "</a></TD>");
					w.write("<TD align=\"center\">" + rsname + "</TD>");

					w.write("</TR>");
				}

				w.write("</TABLE>");
			}

			rs.close();
		}

		// 4. Movies without genres
		private void getMoviesWithoutGenres(Connection dbcon, java.io.BufferedWriter w) throws SQLException, IOException {
			java.sql.PreparedStatement query = dbcon.prepareStatement("SELECT movies.id, movies.title, movies.year FROM movies WHERE movies.id NOT IN (SELECT DISTINCT genres_in_movies.movie_id FROM genres_in_movies);");

			java.sql.ResultSet rs;
			rs = query.executeQuery();

			if (!rs.first())
				w.write("<P>There are no results.</P>");
			else
			{
				w.write("<TABLE WIDTH='100%' BORDER='1'>");

				w.write("<TR>");
				w.write("<TH width=\"15%\">ID</TH>");
				w.write("<TH>Title</TH>");
				w.write("<TH>Year</TH>");
				w.write("</TR>");

				String rsid = rs.getString("movies.id");
				String rstitle = rs.getString("movies.title");
				String rsyear = rs.getString("movies.year");

				w.write("<TR>");

				w.write("<TD align=\"center\" width=\"15%\">" + rsid + "</TD>");
				w.write("<TD align=\"center\"><a href=\"SingleMovie?id=" + rsid + "\">" + rstitle + "</a></TD>");
				w.write("<TD align=\"center\">" + rsyear + "</TD>");

				w.write("</TR>");

				while (rs.next())
				{
					rsid = rs.getString("movies.id");
					rstitle = rs.getString("movies.title");
					rsyear = rs.getString("movies.year");

					w.write("<TR>");

					w.write("<TD align=\"center\" width=\"15%\">" + rsid + "</TD>");
					w.write("<TD align=\"center\"><a href=\"SingleMovie?id=" + rsid + "\">" + rstitle + "</a></TD>");
					w.write("<TD align=\"center\">" + rsyear + "</TD>");

					w.write("</TR>");
				}

				w.write("</TABLE>");
			}

			rs.close();
		}

		// 5. Stars without fn, ln
		private void getStarsWithoutFNLN(Connection dbcon, java.io.BufferedWriter w) throws SQLException, IOException {
			java.sql.PreparedStatement query = dbcon.prepareStatement("SELECT stars.id, stars.first_name, stars.last_name FROM stars WHERE stars.first_name = '' OR stars.last_name = '';");

			java.sql.ResultSet rs;
			rs = query.executeQuery();

			if (!rs.first())
				w.write("<P>There are no results.</P>");
			else
			{
				w.write("<TABLE WIDTH='100%' BORDER='1'>");

				w.write("<TR>");
				w.write("<TH width=\"15%\">ID</TH>");
				w.write("<TH>Name (if applicable)</TH>");
				w.write("</TR>");

				String rsid = rs.getString("stars.id");
				String rsfn = rs.getString("stars.first_name");
				String rsln = rs.getString("stars.last_name");

				if (rsfn.isEmpty() && rsln.isEmpty())
					rsln = " " + rsln;

				w.write("<TR>");

				w.write("<TD align=\"center\" width=\"15%\">" + rsid + "</TD>");
				w.write("<TD align=\"center\"><a href=\"SingleStar?id=" + rsid + "\">" + rsfn + "</a></TD>");

				w.write("</TR>");

				while (rs.next())
				{
					rsid = rs.getString("movies.id");
					rsfn = rs.getString("stars.first_name");
					rsln = rs.getString("stars.last_name");

					if (rsfn.isEmpty() && rsln.isEmpty())
						rsln = " " + rsln;

					w.write("<TR>");

					w.write("<TD align=\"center\" width=\"15%\">" + rsid + "</TD>");
					w.write("<TD align=\"center\"><a href=\"SingleStar?id=" + rsid + "\">" + rsfn + "</a></TD>");

					w.write("</TR>");
				}

				w.write("</TABLE>");
			}

			rs.close();
		}

		// 6. Expired credit cards
		private void getExpiredCCs(Connection dbcon, java.io.BufferedWriter w) throws SQLException, IOException {
			java.sql.PreparedStatement query = dbcon.prepareStatement("SELECT creditcards.id, creditcards.first_name, customers.first_name, creditcards.expiration FROM creditcards JOIN customers ON creditcards.id = customers.cc_id WHERE creditcards.expiration >= CURDATE();");

			java.sql.ResultSet rs;
			rs = query.executeQuery();

			if (!rs.first())
				w.write("<P>There are no results.</P>");
			else
			{
				w.write("<TABLE WIDTH='100%' BORDER='1'>");

				w.write("<TR>");
				w.write("<TH width=\"15%\">ID</TH>");
				w.write("<TH>Name (if applicable)</TH>");
				w.write("</TR>");

				String rsid = rs.getString("stars.id");
				String rsfn = rs.getString("stars.first_name");
				String rsln = rs.getString("stars.last_name");

				if (rsfn.isEmpty() && rsln.isEmpty())
					rsln = " " + rsln;

				w.write("<TR>");

				w.write("<TD align=\"center\" width=\"15%\">" + rsid + "</TD>");
				w.write("<TD align=\"center\"><a href=\"SingleStar?id=" + rsid + "\">" + rsfn + "</a></TD>");

				w.write("</TR>");

				while (rs.next())
				{
					rsid = rs.getString("movies.id");
					rsfn = rs.getString("stars.first_name");
					rsln = rs.getString("stars.last_name");

					if (rsfn.isEmpty() && rsln.isEmpty())
						rsln = " " + rsln;

					w.write("<TR>");

					w.write("<TD align=\"center\" width=\"15%\">" + rsid + "</TD>");
					w.write("<TD align=\"center\"><a href=\"SingleStar?id=" + rsid + "\">" + rsfn + "</a></TD>");

					w.write("</TR>");
				}

				w.write("</TABLE>");
			}

			rs.close();
		}

		// 7. Movies, stars, genres nearly the same (group by id). M: name, year. S: fn, ln, dob
		private void getSimilarMovies(Connection dbcon, java.io.BufferedWriter w) throws SQLException, IOException {
			java.sql.PreparedStatement query = dbcon.prepareStatement("SELECT m1.id, m2.id, m1.title, m2.title, m1.year, m2.year, m1.director, m2.director FROM movies as m1, movies as m2 WHERE m1.id <> m2.id AND m1.title = m2.title AND m1.year = m2.year GROUP BY m1.id;");

			java.sql.ResultSet rs;
			rs = query.executeQuery();

			if (!rs.first())
				w.write("<P>There are no results.</P>");
			else
			{
				w.write("<TABLE WIDTH='100%' BORDER='1'>");

				w.write("<TR>");
				w.write("<TH colspan=\"4\">Movie 1</TH>");
				w.write("<TH colspan=\"4\">Movie 2</TH>");
				w.write("</TR>");

				w.write("<TR>");
				w.write("<TH width=\"10%\">ID</TH>");
				w.write("<TH bgcolor=\"#eee\">Title</TH>");
				w.write("<TH bgcolor=\"#eee\">Year</TH>");
				w.write("<TH>Director</TH>");
				//w.write("<TH>Genres</TH>");
				//w.write("<TH>Stars</TH>");

				w.write("<TH width=\"10%\">ID</TH>");
				w.write("<TH bgcolor=\"#eee\">Title</TH>");
				w.write("<TH bgcolor=\"#eee\">Year</TH>");
				w.write("<TH>Director</TH>");
				//w.write("<TH>Genres</TH>");
				//w.write("<TH>Stars</TH>");
				w.write("</TR>");

				String rsid1 = rs.getString("m1.id");
				String rstitle1 = rs.getString("m1.title");
				String rsyear1 = rs.getString("m1.year");
				String rsdirector1 = rs.getString("m1.director");

				String rsid2 = rs.getString("m2.id");
				String rstitle2 = rs.getString("m2.title");
				String rsyear2 = rs.getString("m2.year");
				String rsdirector2 = rs.getString("m2.director");

				w.write("<TR>");

				w.write("<TD align=\"center\" width=\"10%\">" + rsid1 + "</TD>");
				w.write("<TD bgcolor=\"#eee\" align=\"center\"><a href=\"SingleMovie?id=" + rsid1 + "\">" + rstitle1 + "</a></TD>");
				w.write("<TD bgcolor=\"#eee\" align=\"center\">" + rsyear1 + "</TD>");
				w.write("<TD align=\"center\">" + rsdirector1 + "</TD>");
				//w.write("<TD align=\"center\">" + "genre" + "</TD>");
				//w.write("<TD align=\"center\">" + "stars"+ "</TD>");

				w.write("<TD align=\"center\" width=\"10%\">" + rsid2 + "</TD>");
				w.write("<TD bgcolor=\"#eee\" align=\"center\"><a href=\"SingleMovie?id=" + rsid2 + "\">" + rstitle2 + "</a></TD>");
				w.write("<TD bgcolor=\"#eee\" align=\"center\">" + rsyear2 + "</TD>");
				w.write("<TD align=\"center\">" + rsdirector2 + "</TD>");
				//w.write("<TD align=\"center\">" + "genre" + "</TD>");
				//w.write("<TD align=\"center\">" + "stars"+ "</TD>");

				w.write("</TR>");

				while (rs.next())
				{
					rsid1 = rs.getString("m1.id");
					rstitle1 = rs.getString("m1.title");
					rsyear1 = rs.getString("m1.year");
					rsdirector1 = rs.getString("m1.director");

					rsid2 = rs.getString("m2.id");
					rstitle2 = rs.getString("m2.title");
					rsyear2 = rs.getString("m2.year");
					rsdirector2 = rs.getString("m2.director");

					w.write("<TR>");

					w.write("<TD align=\"center\" width=\"10%\">" + rsid1 + "</TD>");
					w.write("<TD bgcolor=\"#eee\" align=\"center\"><a href=\"SingleMovie?id=" + rsid1 + "\">" + rstitle1 + "</a></TD>");
					w.write("<TD bgcolor=\"#eee\" align=\"center\">" + rsyear1 + "</TD>");
					w.write("<TD align=\"center\">" + rsdirector1 + "</TD>");
					//w.write("<TD align=\"center\">" + "genre" + "</TD>");
					//w.write("<TD align=\"center\">" + "stars"+ "</TD>");

					w.write("<TD align=\"center\" width=\"10%\">" + rsid2 + "</TD>");
					w.write("<TD bgcolor=\"#eee\" align=\"center\"><a href=\"SingleMovie?id=" + rsid2 + "\">" + rstitle2 + "</a></TD>");
					w.write("<TD bgcolor=\"#eee\" align=\"center\">" + rsyear2 + "</TD>");
					w.write("<TD align=\"center\">" + rsdirector2 + "</TD>");
					//w.write("<TD align=\"center\">" + "genre" + "</TD>");
					//w.write("<TD align=\"center\">" + "stars"+ "</TD>");

					w.write("</TR>");
				}

				w.write("</TABLE>");
			}

			rs.close();
		}
		
		private void getSimilarStars(Connection dbcon, java.io.BufferedWriter w) throws SQLException, IOException {
			java.sql.PreparedStatement query = dbcon.prepareStatement("SELECT s1.id, s2.id, s1.first_name, s1.last_name, s2.first_name, s2.last_name, s1.dob, s2.dob FROM stars as s1, stars as s2 WHERE s1.id <> s2.id AND s1.first_name = s2.first_name AND s1.last_name = s2.last_name AND s1.dob = s2.dob GROUP BY s1.id;");

			java.sql.ResultSet rs;
			rs = query.executeQuery();

			if (!rs.first())
				w.write("<P>There are no results.</P>");
			else
			{
				w.write("<TABLE WIDTH='100%' BORDER='1'>");

				w.write("<TR>");
				w.write("<TH colspan=\"3\">Star 1</TH>");
				w.write("<TH colspan=\"3\">Star 2</TH>");
				w.write("</TR>");

				w.write("<TR>");
				w.write("<TH width=\"10%\">ID</TH>");
				w.write("<TH bgcolor=\"#eee\">Name</TH>");
				w.write("<TH bgcolor=\"#eee\">Date of Birth</TH>");
				
				w.write("<TH width=\"10%\">ID</TH>");
				w.write("<TH bgcolor=\"#eee\">Name</TH>");
				w.write("<TH bgcolor=\"#eee\">Date of Birth</TH>");
				w.write("</TR>");

				String rsid1 = rs.getString("s1.id");
				String rsfn1 = rs.getString("s1.first_name");
				String rsln1 = rs.getString("s1.last_name");
				String rsdob1 = rs.getString("s1.dob");
				
				if (!rsfn1.isEmpty() && !rsln1.isEmpty())
					rsln1 = " " + rsln1;

				String rsid2 = rs.getString("s2.id");
				String rsfn2 = rs.getString("s2.first_name");
				String rsln2 = rs.getString("s2.last_name");
				String rsdob2 = rs.getString("s2.dob");
				
				if (!rsfn2.isEmpty() && !rsln2.isEmpty())
					rsln2 = " " + rsln2;

				w.write("<TR>");

				w.write("<TD align=\"center\" width=\"10%\">" + rsid1 + "</TD>");
				w.write("<TD bgcolor=\"#eee\" align=\"center\"><a href=\"SingleStar?id=" + rsid1 + "\">" + rsfn1 + rsln1 + "</a></TD>");
				w.write("<TD bgcolor=\"#eee\" align=\"center\">" + rsdob1 + "</TD>");

				w.write("<TD align=\"center\" width=\"10%\">" + rsid2 + "</TD>");
				w.write("<TD bgcolor=\"#eee\" align=\"center\"><a href=\"SingleStar?id=" + rsid2 + "\">" + rsfn2 + rsln2 + "</a></TD>");
				w.write("<TD bgcolor=\"#eee\" align=\"center\">" + rsdob2 + "</TD>");

				w.write("</TR>");

				while (rs.next())
				{
					rsid1 = rs.getString("s1.id");
					rsfn1 = rs.getString("s1.first_name");
					rsln1 = rs.getString("s1.last_name");
					rsdob1 = rs.getString("s1.dob");
					
					if (!rsfn1.isEmpty() && !rsln1.isEmpty())
						rsln1 = " " + rsln1;

					rsid2 = rs.getString("s2.id");
					rsfn2 = rs.getString("s2.first_name");
					rsln2 = rs.getString("s2.last_name");
					rsdob2 = rs.getString("s2.dob");
					
					if (!rsfn2.isEmpty() && !rsln2.isEmpty())
						rsln2 = " " + rsln2;

					w.write("<TR>");

					w.write("<TD align=\"center\" width=\"10%\">" + rsid1 + "</TD>");
					w.write("<TD bgcolor=\"#eee\" align=\"center\"><a href=\"SingleStar?id=" + rsid1 + "\">" + rsfn1 + rsln1 + "</a></TD>");
					w.write("<TD bgcolor=\"#eee\" align=\"center\">" + rsdob1 + "</TD>");

					w.write("<TD align=\"center\" width=\"10%\">" + rsid2 + "</TD>");
					w.write("<TD bgcolor=\"#eee\" align=\"center\"><a href=\"SingleStar?id=" + rsid2 + "\">" + rsfn2 + rsln2 + "</a></TD>");
					w.write("<TD bgcolor=\"#eee\" align=\"center\">" + rsdob2 + "</TD>");

					w.write("</TR>");
				}

				w.write("</TABLE>");
			}

			rs.close();
		}
		
		private void getSimilarGenres(Connection dbcon, java.io.BufferedWriter w) throws SQLException, IOException {
			java.sql.PreparedStatement query = dbcon.prepareStatement("SELECT g1.id, g2.id, g1.name, g2.name FROM genres as g1, genres as g2 WHERE g1.id <> g2.id AND g1.name = g2.name GROUP BY g1.id;");

			java.sql.ResultSet rs;
			rs = query.executeQuery();

			if (!rs.first())
				w.write("<P>There are no results.</P>");
			else
			{
				w.write("<TABLE WIDTH='100%' BORDER='1'>");

				w.write("<TR>");
				w.write("<TH colspan=\"2\">Genre 1</TH>");
				w.write("<TH colspan=\"2\">Genre 2</TH>");
				w.write("</TR>");

				w.write("<TR>");
				w.write("<TH width=\"10%\">ID</TH>");
				w.write("<TH bgcolor=\"#eee\">Name</TH>");
				
				w.write("<TH width=\"10%\">ID</TH>");
				w.write("<TH bgcolor=\"#eee\">Name</TH>");
				w.write("</TR>");

				String rsid1 = rs.getString("g1.id");
				String rsname1 = rs.getString("g1.name");

				String rsid2 = rs.getString("g2.id");
				String rsname2 = rs.getString("g2.name");

				w.write("<TR>");

				w.write("<TD align=\"center\" width=\"10%\">" + rsid1 + "</TD>");
				w.write("<TD bgcolor=\"#eee\" align=\"center\">" + rsname1 + "</TD>");

				w.write("<TD align=\"center\" width=\"10%\">" + rsid2 + "</TD>");
				w.write("<TD bgcolor=\"#eee\" align=\"center\">" + rsname2 + "</TD>");

				w.write("</TR>");

				while (rs.next())
				{
					rsid1 = rs.getString("g1.id");
					rsname1 = rs.getString("g1.name");

					rsid2 = rs.getString("g2.id");
					rsname2 = rs.getString("g2.name");

					w.write("<TR>");

					w.write("<TD align=\"center\" width=\"10%\">" + rsid1 + "</TD>");
					w.write("<TD bgcolor=\"#eee\" align=\"center\">" + rsname1 + "</TD>");

					w.write("<TD align=\"center\" width=\"10%\">" + rsid2 + "</TD>");
					w.write("<TD bgcolor=\"#eee\" align=\"center\">" + rsname2 + "</TD>");

					w.write("</TR>");
				}

				w.write("</TABLE>");
			}

			rs.close();
		}

		// 8. Birthdate > today, year < 1900
		private void getImproperDOBs(Connection dbcon, java.io.BufferedWriter w) throws SQLException, IOException {
			java.sql.PreparedStatement query = dbcon.prepareStatement("SELECT stars.id, stars.first_name, stars.last_name, stars.dob FROM stars WHERE stars.dob >= CURDATE() OR stars.dob < '1900';");

			java.sql.ResultSet rs;
			rs = query.executeQuery();

			if (!rs.first())
				w.write("<P>There are no results.</P>");
			else
			{
				w.write("<TABLE WIDTH='100%' BORDER='1'>");

				w.write("<TR>");
				w.write("<TH width=\"10%\">ID</TH>");
				w.write("<TH>Name</TH>");
				w.write("<TH>Date of Birth</TH>");
				w.write("</TR>");

				String rsid = rs.getString("stars.id");
				String rsfn = rs.getString("stars.first_name");
				String rsln = rs.getString("stars.last_name");
				String rsdob = rs.getString("stars.dob");
				
				if (!rsfn.isEmpty() && !rsln.isEmpty())
					rsln = " " + rsln;

				w.write("<TR>");

				w.write("<TD align=\"center\" width=\"15%\">" + rsid + "</TD>");
				w.write("<TD align=\"center\">" + rsfn + rsln + "</TD>");
				w.write("<TD align=\"center\">" + rsdob + "</TD>");

				w.write("</TR>");

				while (rs.next())
				{
					rsid = rs.getString("stars.id");
					rsfn = rs.getString("stars.first_name");
					rsln = rs.getString("stars.last_name");
					rsdob = rs.getString("stars.dob");
					
					if (!rsfn.isEmpty() && !rsln.isEmpty())
						rsln = " " + rsln;

					w.write("<TR>");

					w.write("<TD align=\"center\" width=\"15%\">" + rsid + "</TD>");
					w.write("<TD align=\"center\">" + rsfn + rsln + "</TD>");
					w.write("<TD align=\"center\">" + rsdob + "</TD>");

					w.write("</TR>");
				}

				w.write("</TABLE>");
			}

			rs.close();
		}

		// 9. Customer email no @ sign
		private void getImproperEmails(Connection dbcon, java.io.BufferedWriter w) throws SQLException, IOException {
			java.sql.PreparedStatement query = dbcon.prepareStatement("SELECT customers.id, customers.first_name, customers.last_name, customers.email FROM customers WHERE customers.email NOT LIKE '%@%';");

			java.sql.ResultSet rs;
			rs = query.executeQuery();

			if (!rs.first())
				w.write("<P>There are no results.</P>");
			else
			{
				w.write("<TABLE WIDTH='100%' BORDER='1'>");

				w.write("<TR>");
				w.write("<TH width=\"15%\">ID</TH>");
				w.write("<TH>Name</TH>");
				w.write("<TH>Email</TH>");
				w.write("</TR>");

				String rsid = rs.getString("customers.id");
				String rsfn = rs.getString("customers.first_name");
				String rsln = rs.getString("customers.last_name");
				String rsemail = rs.getString("customers.email");
				
				if (!rsfn.isEmpty() && !rsln.isEmpty())
					rsln = " " + rsln;

				w.write("<TR>");

				w.write("<TD align=\"center\" width=\"15%\">" + rsid + "</TD>");
				w.write("<TD align=\"center\">" + rsfn + rsln + "</TD>");
				w.write("<TD align=\"center\">" + rsemail + "</TD>");

				w.write("</TR>");

				while (rs.next())
				{
					rsid = rs.getString("customers.id");
					rsfn = rs.getString("customers.first_name");
					rsln = rs.getString("customers.last_name");
					rsemail = rs.getString("customers.email");
					
					if (!rsfn.isEmpty() && !rsln.isEmpty())
						rsln = " " + rsln;

					w.write("<TR>");

					w.write("<TD align=\"center\" width=\"15%\">" + rsid + "</TD>");
					w.write("<TD align=\"center\">" + rsfn + rsln + "</TD>");
					w.write("<TD align=\"center\">" + rsemail + "</TD>");

					w.write("</TR>");
				}

				w.write("</TABLE>");
			}

			rs.close();
		}



	// Helper functions

	private String displayGenres(Connection dbcon, String movieid) throws SQLException
	{
		java.sql.PreparedStatement query = dbcon.prepareStatement(
				"SELECT genres.name FROM genres "
						+ "JOIN genres_in_movies ON genres.id = genres_in_movies.genre_id "
						+ "JOIN movies ON movies.id = genres_in_movies.movie_id "
						+ "WHERE movies.id = ? "
						+ "ORDER BY genres.name;"
				);

		query.setString(1, movieid);

		ResultSet rs = query.executeQuery();

		StringBuilder s = new StringBuilder();

		while (rs.next())
		{
			String rsgenre = rs.getString("genres.name");
			s.append(rsgenre + "<br /> ");
		}

		return s.toString();
	}
}
