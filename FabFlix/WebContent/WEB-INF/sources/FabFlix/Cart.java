package FabFlix;

import java.io.*;

import javax.annotation.Resource;
import javax.servlet.*;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;

@WebServlet("/Cart")
public class Cart extends HttpServlet
{	private static final long serialVersionUID = 1L;
	@Resource(name="jdbc/TestDB")
    public javax.sql.DataSource myDS;
	private java.util.List<Integer> cartItems = null;

	public static String getADDURL(int MovieID)
	{	return "?addToCart="+MovieID;
	}
	public static String getREMOVEURL(int MovieID)
	{	return "?removeFromCart="+MovieID;
	}
	
	public java.util.List<Integer> getCartItems()
	{	return cartItems;
	}
	public void hook(HttpServletRequest request, HttpServletResponse response)
	{	int toAdd = 0;
		int toRemove = 0;
		{	String toAddS = request.getParameter("addToCart");
			if(null != toAddS) toAdd = Integer.parseInt(toAddS);
		}
		{	String toRemoveS = request.getParameter("removeFromCart");
			if(null != toRemoveS) toRemove = Integer.parseInt(toRemoveS);
		}
		if(toAdd != 0)
		{	initCartItems(request, response);
			cartItems.add(toAdd);
			saveCartItems(request, response);
		}
		if(toRemove != 0)
		{	initCartItems(request, response);
			java.util.List<Integer> cartItems2 = new java.util.ArrayList<Integer>(cartItems);
			for(Integer i: cartItems) if(i.intValue() == toRemove) 
			{	cartItems2.remove(i);
				break;
			}
			cartItems = cartItems2;
			saveCartItems(request, response);
		}
	}

	@SuppressWarnings("unchecked")
	public void initCartItems(HttpServletRequest request, HttpServletResponse response)
	{	if(cartItems != null) return;
		Object cartItemsO = request.getSession().getAttribute("cartItems");
		if(cartItemsO instanceof java.util.List<?>)
			cartItems = (java.util.List<Integer>)cartItemsO;
		if(cartItems == null)
		{	cartItems = new java.util.ArrayList<Integer>();
			saveCartItems(request, response);
		}
	}

	private void saveCartItems(HttpServletRequest request, HttpServletResponse response)
	{	request.getSession().setAttribute("cartItems", cartItems);
	}
    public String getServletInfo()
    {  return "Allow the user to add or remove things from their cart.  " +
    		"When called as a HttpServlet, displays content of cart and allows " +
    		"The user to check out.";
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response)
        throws IOException, ServletException
    {	//new Customer().doLoginAbort(request, response);
    	Customer C = new Customer();
    	C.myDS = myDS;
    	if(C.hook(request, response))
    		return;
    	hook(request, response);
    	initCartItems(request, response);
    	request.setAttribute("C0", this);
    	int Checkout = 0;
		{	String CheckoutS = request.getParameter("checkout");
			if(null != CheckoutS) Checkout = Integer.parseInt(CheckoutS);
		}
    	
    	if(0==Checkout)
    		request.getRequestDispatcher("WEB-INF/Cart.jsp").forward(request, response);
    	else if(1==Checkout)
    		request.getRequestDispatcher("WEB-INF/Checkout1.jsp").forward(request, response);
    }

    public void doPost(HttpServletRequest request, HttpServletResponse response)
        throws IOException, ServletException 
    { doGet(request, response); }
}