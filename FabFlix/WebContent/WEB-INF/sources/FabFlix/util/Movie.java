package FabFlix.util;

import java.io.Serializable;
import java.sql.SQLException;

public class Movie implements Serializable
{	private static final long serialVersionUID = 1L;
	public int id;
	public String title;
	public int year;
	public String director;
	public String banner_url;
	public String trailer_url;
	//public Genre[] genres;
	//public Actor[] actors;
	
	public Movie() {};
	public Movie(int id, java.sql.Connection conn)
	{	try
		{
			conn.prepareStatement("SELECT id, title, year, director, banner_url, trailer_url FROM movies WHERE `id` = ?;");
		} catch (SQLException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	public String getROW( boolean showTitle, boolean showYear, boolean showDirector, boolean showBanner, boolean showTrailer, boolean showId)
	{	String ret = "";
    	ret += "<tr>";
    	if(showId)		ret += "<td>" + id + "</td>";
        if(showTitle)	ret += "<td>" + title + "</td>";
        if(showYear)	ret += "<td>" + year + "</td>";
        if(showDirector)ret += "<td>" + director + "</td>";
        if(showBanner)	ret += "<td>" + banner_url + "</td>";
        if(showTrailer) ret += "<td>" + trailer_url + "</td>";
        ret += "</tr>";

		return ret;
	}

	public int getId()
	{	return id;
	}

	public String getTitle()
	{	return title;
	}

	public int getYear()
	{	return year;
	}

	public String getDirector()
	{	return director;
	}

	public String getBanner_url()
	{	return banner_url;
	}

	public String getTrailer_url()
	{	return trailer_url;
	}
	
}
