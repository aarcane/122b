package FabFlix.util;

public class hooks
{	/***
	 * run hooks
	 * @return If this is true, abort.
	 * @throws IOException 
	 * @throws ServletException 
	 */
	public static boolean runHooks(javax.sql.DataSource myDS, javax.servlet.http.HttpServletRequest request, javax.servlet.http.HttpServletResponse response) throws javax.servlet.ServletException, java.io.IOException
	{	FabFlix.Customer c0 = new FabFlix.Customer();
		c0.myDS = myDS;
		if(c0.hook(request, response))
			return true;
		FabFlix.Cart c1 = new FabFlix.Cart();
		c1.myDS = myDS;
		c1.hook(request, response);

		
		return false;
	}
}
