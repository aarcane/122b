package FabFlix;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
//import java.sql.DriverManager;
//import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.annotation.Resource;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
//import javax.servlet.http.HttpServlet;

@WebServlet("/SingleStar")
public class SingleStar extends HttpServlet
{
	
	private static final long serialVersionUID = 1L;
	@Resource(name="jdbc/TestDB")
    private javax.sql.DataSource myDS;
	
	public String getServletInfo()
    {
       return "Servlet connects to MySQL database and displays result of a SELECT";
    }

    // Use http GET

    public void doGet(HttpServletRequest request, HttpServletResponse response)
        throws IOException, ServletException
    {

        response.setContentType("text/html");    // Response mime type

        // Output stream to STDOUT
        PrintWriter out = response.getWriter();

        out.println("<HTML><HEAD><TITLE>MovieDB</TITLE></HEAD>");
        out.println("<BODY><H1>Single Star</H1>");


        try
           {

              Connection dbcon = myDS.getConnection();
             
              // Declare our statement
              Statement statement = dbcon.createStatement();

              // Select id, first name, last name, dob, photo_url from stars
              int star_id    = Integer.parseInt(request.getParameter("id"));
              

              java.sql.PreparedStatement query = dbcon.prepareStatement
            		  ("SELECT stars.id, stars.first_name, stars.last_name, stars.dob, stars.photo_url FROM stars "
            		  		+ "WHERE id = ?;");
    
              query.setInt(1, star_id);
              
              // Perform the query
              java.sql.ResultSet rs = query.executeQuery();

              out.println("<TABLE border>");

              // Iterate through each row of rs
              while (rs.next())
              {

                  int star_ID     = rs.getInt("id");
                  String first_name  = rs.getString("first_name");
                  String last_name   = rs.getString("last_name");
                  String dateOfBirth = rs.getString("dob");
                  String photo_URL   = rs.getString("photo_url");
                  

                  out.println("<tr>" +
                              "<td>" + star_ID + "</td>" +
                              "<td>" + first_name + "</td>" +
                              "<td>" + last_name + "</td>" +
                              "<td>" + dateOfBirth + "</td>" +
                              "<td><img src = \"" + photo_URL + "\"></td>" +
                              "</tr>"); 
              }

              out.println("</TABLE>");

              rs.close();
              statement.close();
              dbcon.close();
            }
        catch (SQLException ex) {
              while (ex != null) {
                    System.out.println ("SQL Exception:  " + ex.getMessage ());
                    ex = ex.getNextException ();
                }  // end while
            }  // end catch SQLException

        catch(java.lang.Exception ex)
            { throw new ServletException(ex);
//                out.println("<HTML>" +
//                            "<HEAD><TITLE>" +
//                            "MovieDB: Error" +
//                            "</TITLE></HEAD>\n<BODY>" +
//                            "<P>SQL error in doGet: " +
//                            ex.getMessage() + "</P></BODY></HTML>");
                //return;
            }
         out.close();
    }

    public void doPost(HttpServletRequest request, HttpServletResponse response)
        throws IOException, ServletException
    {
	doGet(request, response);
    }
}
