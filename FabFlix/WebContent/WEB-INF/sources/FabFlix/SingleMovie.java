package FabFlix;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
//import java.sql.DriverManager;
//import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.annotation.Resource;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(urlPatterns = {"/SingleMovie", "/SingleMovie/*"})
public class SingleMovie extends HttpServlet
{
	private static final long serialVersionUID = 1L;
	@Resource(name="jdbc/TestDB")
    private javax.sql.DataSource myDS;
	
	/*
	 * (non-Javadoc)
	 * @see javax.servlet.GenericServlet#getServletInfo()
	 * a list of genres (hyperlinked), poster, a list of stars (hyperlinked), and a link to its preview trailer. */
	    public String getServletInfo()
	    {
	       return "Servlet connects to MySQL database and displays result of a SELECT";
	    }

	    // Use http GET

	    public void doGet(HttpServletRequest request, HttpServletResponse response)
	        throws IOException, ServletException
	    {	if(FabFlix.util.hooks.runHooks(myDS, request, response))
	    		return;

	        response.setContentType("text/html");    // Response mime type

	        // Output stream to STDOUT
	        PrintWriter out = response.getWriter();

	        out.println("<HTML><HEAD><TITLE>MovieDB</TITLE></HEAD>");
	        out.println("<BODY><H1>Single Movie</H1>");


	        try
	           {

	              Connection dbcon = myDS.getConnection();
//	              Connection dbcon = DriverManager.getConnection(loginUrl, loginUser, loginPasswd);
	              // Declare our statement
	              Statement statement = dbcon.createStatement();

	              // select id, title, year, director, banner_url, trailer_url from movies
	              Integer movie_id    = 0;
	              String  movie_id_s  = request.getPathInfo();
	              String  movie_id_s1 = request.getParameter("id");
	              if(null!=movie_id_s) try {movie_id = Integer.parseInt(movie_id_s.substring(1)); } catch(NumberFormatException e){}
	              if(null!=movie_id_s1) try {movie_id = Integer.parseInt(movie_id_s1); } catch(NumberFormatException e){}
	              
	              java.sql.PreparedStatement query = dbcon.prepareStatement
	            		  ("SELECT id, title, year, director, banner_url, trailer_url FROM movies "
	            		  		+ "WHERE id = ?;");
	            
	              query.setInt(1, movie_id);
	              
	              java.sql.ResultSet rs = query.executeQuery();
	              
	              out.println("<TABLE border>");

	              // Iterate through each row of rs
	              while (rs.next())
	              {

	                  int	 m_ID         = rs.getInt("id");
	                  String m_Title      = rs.getString("title");
	                  int	 m_year       = rs.getInt("year");
	                  String m_director   = rs.getString("director");
	                  String m_bannerURL  = rs.getString("banner_url");
	                  String m_trailerURL = rs.getString("trailer_url");

	                  out.println("<tr>" +
	                              "<td>" + m_ID + "</td>" +
	                              "<td>" + m_Title + "</td>" +
	                              "<td>" + m_year + "</td>" +
	                              "<td>" + m_director + "</td>" +
	                              "<td><img src=\"" + m_bannerURL + "\"></td>" +
	                              "<td>" + m_trailerURL + "</td>" +
	                              "<td>" + "<a href=\"" + Cart.getADDURL(m_ID) + "\">Add to Cart</a>" + 
	                              "</tr>");
	              }

	              out.println("</TABLE>");

	              rs.close();
	              statement.close();
	              dbcon.close();
	            }
	        catch (SQLException ex) {
	              while (ex != null) {
	                    System.out.println ("SQL Exception:  " + ex.getMessage ());
	                    ex = ex.getNextException ();
	                }  // end while
	            }  // end catch SQLException

	        catch(java.lang.Exception ex)
	            {
	        	    throw new ServletException(ex);
//	                out.println("<HTML>" +
//	                            "<HEAD><TITLE>" +
//	                            "MovieDB: Error" +
//	                            "</TITLE></HEAD>\n<BODY>" +
//	                            "<P>SQL error in doGet: " +
//	                            ex.getMessage() + "</P></BODY></HTML>");
//	                return;
	            }
	         out.close();
	    }

	    public void doPost(HttpServletRequest request, HttpServletResponse response)
	        throws IOException, ServletException
	    {
		doGet(request, response);
	    }
	}
