package FabFlix;

import java.io.*;
import java.sql.*;

import javax.annotation.Resource;
import javax.servlet.*;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;

@WebServlet("/Customer")
public class Customer extends HttpServlet
{
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Resource(name="jdbc/TestDB") 
	public javax.sql.DataSource myDS;
    public Integer ID;
    public String FirstName;
    public String LastName;
    public String Email;
    
    public Integer getID()
    {	return ID;
    }
    public String getFIRSTNAME()
    {	return FirstName;
    }
    public String getLASTNAME()
    {	return LastName;
    }
    public String getEMAIL()
    {	return Email;
    }

    public boolean hook(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
    	HttpSession session = request.getSession();
    	ID = (Integer)session.getAttribute("CustomerID");
    	boolean reqDoLogin = null != request.getParameter("login");
    	boolean reqDoLogout = null != request.getParameter("logout");

    	boolean abort = false;
    	if(reqDoLogout)
    	{	doLogout(request, response);
    	}
    	
    	if(ID != null && reqDoLogin)
    	{	abort = true;
    		throw new RuntimeException("Logged in User attempted to log in again.  Who is this retard?");
    	}
    	
    	if(!abort && ID == null && reqDoLogin)
    	{	ID = doLogin(request, response);
    	}
    	else if(!abort && ID != null)
    	{	fetchCustomerData(request, response);
    	}
    	
    	if(!abort && ID == null)
    	{	abort = true;
    		request.getRequestDispatcher("/WEB-INF/loginPrompt.jsp").forward(request, response);
    	}
    	return abort;
    }
    
    private void doLogout(HttpServletRequest request, HttpServletResponse response)
    {	HttpSession session = request.getSession();
    	session.invalidate();
    	ID=null;
    	FirstName=null;
    	LastName=null;
    	Email=null;
		session = request.getSession(true);    	
    }
    private Integer doLogin(HttpServletRequest request, HttpServletResponse response)
    {	String reqEmail = (String)request.getParameter("email");
    	String reqPassword = (String)request.getParameter("password");
    	if(reqEmail!=null && reqPassword!=null)
    	{	try
    		{	Connection dbcon = myDS.getConnection();
    			PreparedStatement login = dbcon.prepareStatement("SELECT id, first_name, last_name, email FROM `customers` WHERE `email`=? AND `password`=? LIMIT 1;");
    			login.setString(1, reqEmail);
    			login.setString(2, reqPassword);
    			ResultSet isLoggedIn = login.executeQuery();
    			if(isLoggedIn.next())
    			{   ID = isLoggedIn.getInt(1);
    				FirstName = isLoggedIn.getString(2);
    				LastName = isLoggedIn.getString(3);
    				Email = isLoggedIn.getString(4);
    				request.getSession(true).setAttribute("CustomerID", ID);
    			}
    		} catch (SQLException e)
    		{e.printStackTrace();
    		}
    	}
    	return ID;
    	
    }

    private void fetchCustomerData(HttpServletRequest request, HttpServletResponse response)
    {	try
    	{	Connection dbcon = myDS.getConnection();
    		PreparedStatement login = dbcon.prepareStatement("SELECT id, first_name, last_name, email FROM `customers` WHERE `id` = ? LIMIT 1;");
    		login.setInt(1, ID);
    		ResultSet isLoggedIn = login.executeQuery();
    		if(isLoggedIn.next())
    		{   ID = isLoggedIn.getInt(1);
    			FirstName = isLoggedIn.getString(2);
    			LastName = isLoggedIn.getString(3);
    			Email = isLoggedIn.getString(4);
    			request.getSession().setAttribute("CustomerID", ID);
    		}
    	} catch (SQLException e)
    	{	e.printStackTrace();
    }

    	
    }
    public String getServletInfo()
    {  return "Processes login, logout, and other requests on each page (load)." +
    		"Displays a login page if the user is not logged in.";
    }

    // Use http GET

    public void doGet(HttpServletRequest request, HttpServletResponse response)
        throws IOException, ServletException
    {
    	if(hook(request, response))
    		return;
    	request.setAttribute("C0", this);
    	request.getRequestDispatcher("/WEB-INF/BrowseSearch.jsp").include(request, response);
        
    }

    public void doPost(HttpServletRequest request, HttpServletResponse response)
        throws IOException, ServletException
    {
	doGet(request, response);
    }
}